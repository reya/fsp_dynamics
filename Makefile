HDF5=/opt/hdf5
#/opt/hdf5
MPI=/opt/openmpi_intel
MKL=/opt/intel/mkl
LIBRARIES=/opt/sundials/lib:${MPI}/lib:${HDF5}/lib:/opt/intel/mkl/lib/intel64:/opt/intel/mkl/lib
INCLUDE=/opt/sundials/include:${MPI}/include:${HDF5}/include:/opt/slepc/include:/opt/petsc/includfe
SUNDIALSLIBS = -lsundials_fkinsol -lsundials_kinsol -lsundials_fcvode -lsundials_fnvecserial -lsundials_fnvecparallel -lsundials_cvode -lsundials_nvecserial -lsundials_nvecparallel
#MODULES=main.f90 functions.f90 linalg.f90 printing.f90 testing.f90 subset.f90
SOURCES= dynamics_basis.f90 dynamics_operators.f90 dynamics_prepare.f90 dynamics_propagation.f90 dynamics_errchk.f90 dynamics_rtlib.f90 dynamics_datatypes.f90 dynamics_conversions.f90 dynamics_io.f90 ../fsp_v2/utils.f90 ../fsp_v2/basis_set.f90 ../fsp_v2/io.f90  ../fsp_v2/types.f90 ../fsp_v2/rtlib.f90 ../fsp_v2/subset.f90 #quadpack/*.f 
#CSOURCES=slepc_eigsolver.c io_hdf.c
OBJECTS=$(SOURCES:.f90=.o)
EXECUTABLE=dynamics.x
EXEC_SOURCE=dynamics.f90
EXEC_OBJ=$(EXEC_SOURCE:.f90=.o)
#SOURCES_ANALYSIS=dynamics_errchk.f90 dynamics_rtlib.f90 dynamics_datatypes.f90 dynamics_conversions.f90 dynamics_io.f90  extract_dm_rt.f90 extract_dm_io.f90 extract_dm_evolution.f90
SOURCES_ANALYSIS=extract_dm_rt.f90 extract_dm_io.f90 extract_dm_evolution.f90
OBJECTS_ANALYSIS=$(SOURCES_ANALYSIS:.f90=.o)
#FCFLAGS = -L${HDF5}/lib -I${HDF5}/include -lhdf5_fortran -I$(MKL)/include -I$(MKL)/include/intel64/ilp64 -L/opt/intel/mkl/lib/intel64 -lmkl_blas95_ilp64 -lmkl_lapack95_ilp64 -lmkl_intel_ilp64 -lmkl_core -lmkl_intel_thread -lpthread -liomp5 -lm -Wl,-rpath,${HDF5}/lib:/opt/intel/mkl/lib/intel64

### + ilp64
FCFLAGS = -I$(MKL)/include -I$(MKL)/include/intel64/ilp64 -I${MPI}/include -L${MPI}/lib -I${HDF5}/include -L${HDF5}/lib -lhdf5_fortran -lhdf5hl_fortran -I/opt/petsc/include -I/opt/slepc/include -L/opt/intel/lib/ -L/opt/intel/mkl/lib/intel64 -L. -lmkl_blas95_ilp64 -lmkl_lapack95_ilp64 -lmkl_intel_ilp64 -lmkl_core -lmkl_rt -lmkl_intel_thread -L/opt/sundials/lib -I/opt/sundials/include ${SUNDIALSLIBS} -Wl,-rpath,${HDF5}/lib:/opt/intel/mkl/lib/intel64:/opt/intel/mkl/lib:${MPI}/lib:${HDF5}/lib:${HDF5}/include:/opt/sundials/lib:/opt/sundials/include
#FCFLAGS = -I${INCLUDE} -L${LIBRARIES} -lhdf5_fortran -lhdf5hl_fortran -L. -lmkl_blas95_ilp64 -lmkl_lapack95_ilp64 -lmkl_intel_ilp64 -lmkl_core -lmkl_rt -lmkl_intel_thread ${SUNDIALSLIBS} -Wl,-rpath,${LIBRARIES}:${INCLUDE}

###  + lp64
#FCFLAGS = -I$(MKL)/include -I$(MKL)/include/intel64/lp64 -L/opt/intel/lib/ -L/opt/intel/mkl/lib/intel64 -lmkl_blas95_lp64 -lmkl_lapack95_lp64 -lmkl_intel_lp64 -lmkl_core -lmkl_intel_thread  -liomp5 -Wl,-rpath,${HDF5}/lib:/opt/intel/mkl/lib/intel64:/opt/intel/mkl/lib

###  + lp64 sequential
#FCFLAGS = -I$(MKL)/include -I$(MKL)/include/intel64/lp64 -L/opt/intel/lib/ -L/opt/intel/mkl/lib/intel64 -lmkl_blas95_lp64 -lmkl_lapack95_lp64 -lmkl_intel_lp64 -lmkl_core -lmkl_sequential -Wl,-rpath,${HDF5}/lib:/opt/intel/mkl/lib/intel64:/opt/intel/mkl/lib

###  + 32bit sequential
#FCFLAGS = -I$(MKL)/include -I$(MKL)/include/ia32/ -L/opt/intel/lib/ -L/opt/intel/mkl/lib/ia32 -lmkl_blas95 -lmkl_lapack95 -lmkl_intel -lmkl_core -lmkl_solver -lmkl_sequential -Wl,-rpath,${HDF5}/lib:/opt/intel/mkl/lib/ia32:/opt/intel/mkl/lib



#FCFLAGS = -I$(MKL)/include -I$(MKL)/include/intel64/ilp64 -L/opt/intel/mkl/lib/intel64 -lmkl_blas95_ilp64 -lmkl_lapack95_ilp64 -lmkl_intel_ilp64 -lmkl_core -lmkl_sequential -lm -Wl,-rpath,${HDF5}/lib:/opt/intel/mkl/lib/intel64

#-lhdf5_fortran -I/opt/intel/mkl/include -L/opt/intel/mkl/lib/intel64 -lmkl_blas95_lp64 -lmkl_lapack95_lp64 -lmkl_core -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_rt -Wl,-rpath,${HDF5}/lib:/opt/petsc/lib:/opt/mvapich/lib:/opt/slepc/lib:/opt/intel/mkl/lib/intel64
#CC = mpicc -O0 -g -w $(CFLAGS) 
########################
### Optimize heavily ###
########################
#FC = mpif90 -fpp -mkl -heap-arrays -O3 -ipo -opt-report=5 #-traceback -fp-stack-check  -O0 -g -check all #-check bounds #-openmp #-gen-interfaces -warn interfaces
#FC = mpif90 -fpp -mkl -heap-arrays -O3 -guide -parallel -opt-report=5 #-traceback -fp-stack-check  -O0 -g -check all #-check bounds #-openmp #-gen-interfaces -warn interfaces

#FC = mpif90 -fpp -mkl -heap-arrays -O0 -g -ipo -traceback -check all -check noarg_temp_created  #-traceback -fp-stack-check  -O0 -g -check all #-check bounds #-openmp #-gen-interfaces -warn interfaces

########################
### CHECK EVERYTHING ###
########################
FCDEBUG = ${MPI}/bin/mpifort -fpp -mkl -heap-arrays -traceback -fp-stack-check  -O0 -g -check noarg_temp_created -check all -fp-model strict
FCOPT = ${MPI}/bin/mpifort -fpp -openmp -mkl -heap-arrays -O3 -ipo -xHost -fp-model strict -diag-disable 8291 -diag-disable 8293 -diag-disable 11021

FC = ${FCOPT}
#FC = ${FCDEBUG}


all: $(SOURCES) ${EXEC_SOURCE} ${OBJECTS} ${EXEC_OBJ} $(EXECUTABLE)

analysis:  ${SOURCES_ANALYSIS} 
	$(FC) ${FCFLAGS} -c ${SOURCES_ANALYSIS} 
	${FC} ${FCFLAGS} io.o utils.o dynamics_rtlib.o ${OBJECTS_ANALYSIS}  -o extract_dm_evolution.x
#%.o: %.f90
#	${FC} -c ${SOURCES} ${EXEC_SOURCE} ${SOURCES_ANALYSIS} ${FCFLAGS} 
${OBJECTS}: ${SOURCES}
	${FC} ${FCFLAGS} -c ${SOURCES} 

${EXEC_OBJ}: ${EXEC_SOURCE}
	${FC} ${FCFLAGS} -c ${EXEC_SOURCE} 

$(EXECUTABLE): ${OBJECTS} ${EXEC_OBJ}
	${FC} ${OBJECTS} ${EXEC_OBJ} $(FCFLAGS) -o $@

ex:
	$(FC) -c ${SOURCES} ${FCFLAGS}   
	$(FC) $(OBJECTS) $(FCFLAGS) -o $(EXECUTABLE)
tags:
	etags `find . -name "*.f90"`
clean:
	rm *.x *.o *.mod
