program dynamics
  use dynamics_prepare
  use dynamics_propagation
  use dynamics_rtlib, only: rt
  implicit none 

  write(*,*) " "
  write(*,*) " * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *"
  write(*,*) " * * * Reduced Density Matrix dynamics with dissipation * * * "
  write(*,*) " * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *"
  write(*,*) " "

  ! # Set up everything, read the input and so on #
  call prepareDynamics()

  if (rt%integration==2) then
     call propagateDM_CN()
  else if (rt%integration==1) then
     call propagateDM()
  else
     write(*,*) " * Input error: unknown integration scheme"
     stop
  end if
  ! #
  write(*,*) " "
  write(*,*) " * DONE *"
  write(*,*) " "
  write(*,*) " EXIT FROM THE MAIN PROGRAM "
  write(*,*) " "
  
end program dynamics

subroutine FKFUN(U,FVAL,IER)
  use dynamics_datatypes, only: dp
  use dynamics_operators
  use dynamics_rtlib, only : psize
  use dynamics_propagation, only: time_step_CN, time_step_CN_Y0, Y0, NSTEPS_CN, RMSE_CN, previous_time_computed, Fcurrent
  implicit none 
  real(kind=dp), dimension(psize), intent(in) :: U
  real(kind=dp), dimension(psize), intent(out) :: FVAL
  real(kind=dp), dimension(psize) :: Fnext
  real(kind=dp) :: dt_step
  integer, intent(out) :: IER

  if (.not.(previous_time_computed)) then
     Fcurrent(:) = 0.0d00
     call RightHandSide(time_step_CN_Y0,Y0,Fcurrent)
     previous_time_computed = .TRUE.
  end if
  
  call RightHandSide(time_step_CN,U,Fnext)
  dt_step = time_step_CN-time_step_CN_Y0
  FVAL = U - Y0 - 0.5d00*dt_step*(Fnext+Fcurrent)
  RMSE_CN = sum(FVAL(:)**2)
  NSTEPS_CN = NSTEPS_CN + 1
!  write(*,"(A,F16.10)") "        RMSE = ", sqrt(sum(FVAL(:)**2))
  IER = 0
end subroutine FKFUN

subroutine FCVFUN(T, Y, YDOT, IPAR, RPAR, IER)
  use dynamics_datatypes, only: dp
  use dynamics_rtlib, only: psize
  use dynamics_operators
  implicit none 
  real(kind=dp), dimension(psize), intent(in) :: Y
  real(kind=dp), dimension(psize), intent(out) :: YDOT
  real(kind=dp), dimension(1), intent(in) :: RPAR
  real(kind=dp), intent(in)         :: t
  integer, dimension(1), intent(in) :: IPAR
  integer :: i, j, k
  integer, intent(out) :: IER
  call RightHandSide(T,Y,YDOT)
  IER = 0
end subroutine FCVFUN

subroutine RightHandSide(T,Y,YDOT)
  use dynamics_datatypes, only: dp
  use dynamics_rtlib, only: psize, rt
  use dynamics_operators
  implicit none 
  real(kind=dp), dimension(psize), intent(in) :: Y
  real(kind=dp), dimension(psize), intent(out) :: YDOT
  real(kind=dp), intent(in)         :: t
  rt%number_of_steps = rt%number_of_steps + 1
  if (rt%full_secular==0) then
     call RightHandSide_full(T,Y,YDOT)
  else if (rt%full_secular==1) then
     call RightHandSide_Kuhn(T,Y,YDOT)
  else if (rt%full_secular==2) then
     call RightHandSide_Polyutov(T,Y,YDOT)
  else if (rt%full_secular==3) then
     call RightHandSide_correct(T,Y,YDOT)
  else
     write(*,*) " [ERROR] Unknown right-hand side: ", rt%full_secular
     stop
  end if
end subroutine RightHandSide
