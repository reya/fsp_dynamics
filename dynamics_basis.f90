module dynamics_basis
  use dynamics_errchk
  use dynamics_rtlib
  use subset
  implicit none

  type(basis_function_type), allocatable, protected, dimension(:) :: basis
  integer :: basis_dim
  integer :: basis_set_size
  logical :: basis_set_initialized=.FALSE.
  logical :: isInitialized=.FALSE.
  
!  public :: initializeBasisSet
  public :: basis
  public :: initialize_basis_set_v2
  
  private
  
contains


  subroutine initialize_basis_set_v2(basis_size,compute, print_output, Ng, Ne, Nmodes, Nmers, Nparticle)
    integer, intent(inout) :: basis_size
    integer, intent(in)    :: Nmodes, Nparticle, Nmers
    integer, intent(in), dimension(Nmodes)    :: Ng, Ne
    logical, intent(in)    :: compute
    integer :: i, j, k, depth, bcount
    ! Dirty shit comes below.
    integer :: i1, i2, i3, i4, i5, npcount, iexc, ierr
    integer, dimension(Nmodes) :: vibexc1, vibexc2, vibexc3, vibexc4, vibexc5
    ! End of dirty shit for now.
    integer, allocatable, dimension(:) :: nparticles, gs_particles
    integer, allocatable, dimension(:) :: idx, pos, pos_gs, idx_gs
    logical, allocatable, dimension(:) :: more
    logical, optional, intent(in)      :: print_output
    logical :: print_output_in
    logical :: gs_fine
    vibexc1(:) = 0; vibexc2(:) = 0; vibexc3(:) = 0; vibexc4(:) = 0; vibexc5(:) = 0
    if (present(print_output)) then
       print_output_in = print_output
    else
       if (compute) then
          print_output_in = .FALSE.
       else
          print_output_in = .TRUE.
       end if
    end if
    
    write(*,"(A,I2,A)") " Pre-computing size of the ", Nparticle," basis set"
    !# First: run the Nparticle loop for (100), (010), etc.
    allocate(nparticles(Nparticle))
    allocate(more(Nparticle))
    if (Nparticle>1) then
       allocate(gs_particles(Nmers))
       allocate(pos_gs(Nmers))
    end if
    
    nparticles(:) = 0
    if (compute) then
       write(*,*) " Filling components of the basis set: "
       allocate(basis(basis_size))
       basis_set_size = basis_size
       bcount = 0
    else
       basis_size = 0
    end if
    
    do iexc=1,Nmers
       if (print_output_in) then
          write(*,*) "     ================================================= "
          write(*,*) "       One-particle contribution: Excited state on ", iexc
       end if
       ! # Scan Nparticle depth
       ! OK, *dirty shit* comes in.
       ! But we don't need more than 5-paricle basis for sure.


       !write(*,*) "       Excitation on: ", iexc, "  Vibrational exc in GS = 0"
       more(:) = .FALSE.
       do
          call index_next0(Nmodes,maxval(Ne),vibexc1,more(1))
          if (all(vibexc1<=Ne)) then
             if (print_output_in) then
                write(*,*) "    (bs)>  (1) Monomer = ", iexc, " e = ",vibexc1(:)-1
             end if

             if (compute) then
                bcount = bcount + 1
                allocate(basis(bcount)%particle(1),stat=ierr); call chkmerr(ierr)
                basis(bcount)%np = 1
                allocate(basis(bcount)%particle(1)%vib(Nmodes),stat=ierr); call chkmerr(ierr)
                basis(bcount)%particle(1)%vib(:) = vibexc1(:) - 1
                basis(bcount)%particle(1)%idx    = iexc
             else
                basis_size = basis_size + 1
             end if
          end if
          if (.not.(more(1))) exit
       end do
       if (print_output_in) then
          write(*,*) " "
       end if
       
       if (Nparticle>1) then
          gs_particles(:) = 0
          do while (sum(gs_particles(:))<Nmers)
             ! (0,0,1,1,0,etc.) where position is the monomer, and 1/0 is the excitation.
             call binary_vector_next(Nmers, gs_particles)
             gs_fine = .TRUE.
             !               write(*,*) " gs_particles = ", gs_particles
             do i=1,Nmers
                if ((gs_particles(i)==1).and.(i==iexc)) then
                   !                      write(*,*) " Exiting the loop with ", gs_particles
                   gs_fine = .FALSE.
                end if
             end do
             if (gs_fine) then
                k=0
                pos_gs(:) = 0
                do i=1,Nmers
                   if (gs_particles(i)==1) then
                      k = k + 1
                      pos_gs(k) = i ! 
                   end if
                end do
                !write(*,*) "     Next: ", gs_particles 
                !write(*,*) "       E: ", iexc, "  G: ", pos_gs(1:k)

                ! ### So, here I put the select block ###
                ! ### To iterate over vibrational exc ###
                npcount = k ! Number of monomers in gs
                more(:) = .FALSE.

                if (npcount<Nparticle) then  ! npcount is only for gs. So it must be less than total Nparticle.
                   select case(npcount)
                   case(1)   ! Two-partcle basis
                      if (print_output_in) then
                         write(*,*) "     ================================================= "
                         write(*,*) "       Two-particle contribution: Excited state on ", iexc
                      end if
                      do
                         call index_next0(Nmodes,maxval(Ne),vibexc1,more(1))
                         if (all(vibexc1<=Ne)) then
                            if (print_output_in) then
                               write(*,*) "    >> (2) Monomer = ", iexc, " e = ",vibexc1(:)-1
                            end if

                            do
                               call index_next0(Nmodes,maxval(Ng),vibexc2,more(2))
                               if (all(vibexc2<=Ng)) then
                                  if (sum(vibexc2-1)>=1) then
                                     if (print_output_in) then
                                        write(*,*) "     (bs)> (2) Monomer = ", pos_gs(1), " g = ",vibexc2(:)-1
                                     end if

                                     if (compute) then
                                        bcount = bcount + 1
                                        allocate(basis(bcount)%particle(2),stat=ierr); call chkmerr(ierr)
                                        basis(bcount)%np = 2
                                        allocate(basis(bcount)%particle(1)%vib(Nmodes),stat=ierr); call chkmerr(ierr)
                                        allocate(basis(bcount)%particle(2)%vib(Nmodes),stat=ierr); call chkmerr(ierr)
                                        basis(bcount)%particle(1)%vib(:) = vibexc1(:) - 1
                                        basis(bcount)%particle(2)%vib(:) = vibexc2(:) - 1
                                        basis(bcount)%particle(1)%idx    = iexc
                                        basis(bcount)%particle(2)%idx    = pos_gs(1)
                                     else
                                        basis_size = basis_size + 1
                                     end if
                                  end if
                               end if
                               if (.not.more(2)) exit
                            end do
                            if (print_output_in) then
                               write(*,*) " "
                            end if
                         end if

                         if (.not.more(1)) exit
                      end do
                   case(2)   ! Three-partcle basis
                      if (print_output_in) then
                         write(*,*) "     ================================================= "
                         write(*,*) "       Three-particle contribution: Excited state on ", iexc
                      end if
                      do
                         call index_next0(Nmodes,maxval(Ne),vibexc1,more(1))
                         if (all(vibexc1<=Ne)) then
                            if (print_output_in) then
                               write(*,*) "    >> (3) Monomer = ", iexc, " e = ",vibexc1(:)-1
                            end if

                            do
                               call index_next0(Nmodes,maxval(Ng),vibexc2,more(2))
                               if (all(vibexc2<=Ng)) then
                                  if (sum(vibexc2-1)>=1) then
                                     if (print_output_in) then
                                        write(*,*) "     (bs)> (3) Monomer = ", pos_gs(1), " g = ",vibexc2(:)-1
                                     end if
                                     do
                                        call index_next0(Nmodes,maxval(Ng),vibexc3,more(3))
                                        if (all(vibexc3<=Ng)) then
                                           if (sum(vibexc3-1)>=1) then
                                              if (print_output_in) then
                                                 write(*,*) "     (bs)> (3) Monomer = ", pos_gs(2), " g = ",vibexc3(:)-1
                                              end if
                                              if (compute) then
                                                 bcount = bcount + 1
                                                 allocate(basis(bcount)%particle(3),stat=ierr); call chkmerr(ierr)
                                                 basis(bcount)%np = 3
                                                 allocate(basis(bcount)%particle(1)%vib(Nmodes),stat=ierr); call chkmerr(ierr)
                                                 allocate(basis(bcount)%particle(2)%vib(Nmodes),stat=ierr); call chkmerr(ierr)
                                                 allocate(basis(bcount)%particle(3)%vib(Nmodes),stat=ierr); call chkmerr(ierr)
                                                 basis(bcount)%particle(1)%vib(:) = vibexc1(:) - 1
                                                 basis(bcount)%particle(2)%vib(:) = vibexc2(:) - 1
                                                 basis(bcount)%particle(3)%vib(:) = vibexc3(:) - 1
                                                 basis(bcount)%particle(1)%idx    = iexc
                                                 basis(bcount)%particle(2)%idx    = pos_gs(1)
                                                 basis(bcount)%particle(3)%idx    = pos_gs(2)
                                              else
                                                 basis_size = basis_size + 1
                                              end if
                                           end if
                                        end if
                                        if (.not.more(3)) exit
                                     end do
                                  end if
                               end if
                               if (.not.more(2)) exit
                            end do
                            if (print_output_in) then
                               write(*,*) " "
                            end if
                         end if

                         if (.not.more(1)) exit
                      end do
                   case default
                      write(*,*) " [BASIS_SET] Error: Unexpected number of GS particles"
                      write(*,*) "                    npcount = ",npcount

                   end select
                   ! ### END OF SODOMY ###
                   if (print_output_in) then
                      write(*,*) "  "
                   end if

                end if
             end if
          end do
          if (print_output_in) then
             write(*,*) "  * * *  "
          end if
       end if
       if (allocated(pos)) deallocate(pos)
    end do
    if (print_output_in) then
       write(*,*) " FINALLY: size of the basis set is ", basis_size
       
    end if
    if (compute) then
       isInitialized=.TRUE.
       write(*,*) " BASIS SET INITIALIZED"
    end if
  end subroutine initialize_basis_set_v2
  
  ! # First call: find out the size of the basis set
  ! # Second call: allocate and compute the actual elements of the basis set
  ! subroutine initializeBasisSet(basis_allocated, Ng, Ne, Nmodes, Nmers)
  !   integer, intent(in) :: Ng, Ne, Nmodes, Nmers
  !   logical, intent(in) :: basis_allocated 
  !   integer :: i, j, k, im, M, N, info
  !   integer :: imers, jmers, mng, nng
  !   integer, dimension(Nmodes) :: idx_ematrix
  !   integer, dimension(Ng*Ne,2) :: index_a
  !   logical more, display_basis 

  !   display_basis=.FALSE.
  !   !display_basis=.TRUE.
  !   ! First, define all purely electronic basis functions between every pair of monomers
  !   call isInitialized()
  !   if (.not. basis_allocated) then
  !      basis_dim = 0
  !      !     gbdim = 0
  !   else
  !      allocate(basis_set(basis_dim),stat=info); call chkmerr(info);
  !      basis_set_initialized = .TRUE.
  !      do i=1,basis_dim
  !         allocate(basis_set(i)%basis(Nmodes),stat=info); call chkmerr(info);
  !      end do
  !      write(*,"(A,I8)") " [dynamics_basis] Diabatic (monomers) basis set size: ", basis_dim 
  !   end if
    


  !   more = .FALSE.
  !   idx_ematrix(:)=0
  !   k=0
  !   ! if (display_basis.and.basis_allocated) then
  !   !    print *, " "
  !   !    print "(A)", " ==================== "
  !   !    print "(A)", "  GROUND STATE BASIS "
  !   !    print "(A)", " ==================== "
  !   !    print *, " "
  !   ! end if

  !   ! do
  !   !    call index_next0(Nmodes, Ng, idx_ematrix, more)
  !   !    k=k+1
  !   !    if (.not. basis_allocated) then
  !   !       gbdim = gbdim + 1
  !   !    else
  !   !       write(*,*) "Ground state basis function # ", k
  !   !       do im=1,Nmodes
  !   !          gbset(k)%gbasis(im)%g=idx_ematrix(im)-1
  !   !          write(*,"(A,I3,A,I3)") "Mode = ", im, "|g> = ",gbset(k)%gbasis(im)%g
  !   !       end do

  !   !    end if
  !   !    if (.not. more) exit
  !   ! end do


  !   if (display_basis.and.basis_allocated) then
  !      print *, " "
  !      print "(A)", " ===================== "
  !      print "(A)", "  EXCITED STATE BASIS "
  !      print "(A)", " ===================== "
  !      print *, " "
  !   end if

  !   idx_ematrix(:)=0
  !   k=1
  !   do i=1,Ng
  !      do j=1,Ne
  !         index_a(k,:)=(/i-1,j-1/)
  !         k=k+1
  !      end do
  !   end do



  !   k=1
  !   do M=1,Nmers 
  !      do N=1,Nmers
  !         if (M.ne.N) then
  !            if (display_basis.and.basis_allocated) then
  !               write(*,"(A82)") " ================================================================================ "
  !               write(*,*) " * Basis function #",k
  !            end if
  !            if (basis_allocated) then
  !               do im=1,Nmodes
  !                  basis_set(k)%basis(im)%g=0
  !                  basis_set(k)%basis(im)%e=0
  !                  basis_set(k)%basis(im)%gidx=M
  !                  basis_set(k)%basis(im)%eidx=N
  !                  basis_set(k)%basis(im)%midx=im
  !                  if (display_basis) then
  !                     write(*,"(A,I2,A,I2,A,I2,A,I2,A,I2,A,I2,A)") "Mode = ", basis_set(k)%basis(im)%midx, " |g_i,M> * |e_j,N>:  |",&
  !                          basis_set(k)%basis(im)%g," :: ",&
  !                          basis_set(k)%basis(im)%gidx,"> * |", basis_set(k)%basis(im)%e,&
  !                          " :: ", basis_set(k)%basis(im)%eidx,">"
  !                  end if
  !               end do
  !               k=k+1
  !            else
  !               basis_dim=basis_dim+1
  !            end if
  !         end if
  !      end do
  !   end do

  !   ! Next run through pairs of excited states if we have vibrational excitations
  !   if ((Ng.gt.1).or.(Ne.gt.1)) then
  !      more = .FALSE.
  !      idx_ematrix(:)=0
  !      do
  !         call index_next0(Nmodes, Ng*Ne, idx_ematrix, more)
  !         if (sum(idx_ematrix-1)>0) then

  !            do M=1,Nmers
  !               do N=1,Nmers
  !                  if (N.ne.M) then
  !                     if (basis_allocated) then

  !                        if (display_basis) then
  !                           write(*,"(A82)") " ================================================================================ "
  !                           write(*,*) " * Basis function #",k
  !                        end if

  !                        do im=1,Nmodes
  !                           basis_set(k)%basis(im)%g=index_a(idx_ematrix(im),1)
  !                           basis_set(k)%basis(im)%e=index_a(idx_ematrix(im),2)
  !                           basis_set(k)%basis(im)%gidx=M
  !                           basis_set(k)%basis(im)%eidx=N
  !                           basis_set(k)%basis(im)%midx=im

  !                           if (display_basis) then
  !                              write(*,"(A,I2,A,I2,A,I2,A,I2,A,I2,A,I2,A)") "Mode = ", basis_set(k)%basis(im)%midx, " |g_i,M> * |e_j,N>:  |",&
  !                                   basis_set(k)%basis(im)%g," :: ",&
  !                                   basis_set(k)%basis(im)%gidx,"> * |", basis_set(k)%basis(im)%e,&
  !                                   " :: ", basis_set(k)%basis(im)%eidx,">"
  !                           end if

  !                        end do
  !                        k=k+1
  !                     else
  !                        basis_dim = basis_dim+1
  !                     end if
  !                  end if
  !               end do
  !            end do

  !         end if
  !         if (.not. more) exit
  !      end do
  !   end if
  !   if (.not. basis_allocated) then
  !      return
  !   end if
  ! end subroutine initializeBasisSet

  
  ! subroutine isInitialized()
  !   if (basis_set_initialized) then
  !      write(*,*) "ERROR in initializeBasisSet: you are not supposed to call it &
  !           after the basis set is allocated and initialized"
  !      stop
  !   end if
  ! end subroutine isInitialized
  

end module dynamics_basis
