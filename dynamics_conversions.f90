module dynamics_conversions
  use dynamics_datatypes, only: dp
  use utils
  implicit none
  real(kind=dp),parameter :: kB=3.16681e-6 ! Boltzmann constant in Eh/K
  
  ! public :: bohr_to_ang, ang_to_bohr, ha_to_ev, ev_to_ha
  ! public :: ha_to_rcm, rcm_to_ha
  public :: bosefun, dbosefun, time_au_to_fs
  private
  
contains
  
  ! elemental real(kind=dp) function bohr_to_ang(x)
  !   implicit none
  !   real(kind=dp), intent(in) :: x
  !   bohr_to_ang = x*0.5291772109217
  ! end function bohr_to_ang
  
  ! elemental real(kind=dp) function ang_to_bohr(x)
  !   implicit none
  !   real(kind=dp), intent(in) :: x
  !   ang_to_bohr = x/0.5291772109217
  ! end function ang_to_bohr
  
  
  ! elemental real(kind=dp) function ha_to_ev(x)
  !   implicit none
  !   real(kind=dp), intent(in) :: x
  !   ha_to_ev = x*27.2113850560
  ! end function ha_to_ev

  ! elemental real(kind=dp) function ev_to_ha(x)
  !   implicit none
  !   real(kind=dp), intent(in) :: x
  !   ev_to_ha = x/27.2113850560
  ! end function ev_to_ha

  ! elemental real(kind=dp) function ha_to_rcm(x)
  !   real(kind=dp), intent(in) :: x
  !   ha_to_rcm = x*219474.631370515D00 ! TODO 313515 -> 702000
  ! end function ha_to_rcm

  ! elemental real(kind=dp) function rcm_to_ha(x)
  !   real(kind=dp), intent(in) :: x
  !   rcm_to_ha = x/219474.631370515D00 ! TODO 313515 -> 702000
  ! end function rcm_to_ha
  elemental real(kind=dp) function time_au_to_fs(x)
    real(kind=dp), intent(in) :: x
    time_au_to_fs = x*2.418884326509e-2
  end function time_au_to_fs
  

  real(kind=dp) function bosefun(E,mu,T)
    real(kind=dp), intent(in) :: E
    real(kind=dp), intent(in) :: mu, T ! T in K, mu and E in Hartree
    if ((E-mu)==0.0d00) then
       write(*,"(A)") " [bosefun] ERROR: E-mu=0.0 will give Inf"
       stop
    end if
    bosefun = 1.0/(exp((E-mu)/(kB*T))-1.0)
  end function bosefun

  real(kind=dp) function dbosefun(E,mu,T)
    real(kind=dp), intent(in) :: E
    real(kind=dp), intent(in) :: mu, T ! T in K, mu and E in Hartree
    dbosefun = (exp((E-mu)/(kB*T)))/(kB*T*(exp((E-mu)/(kB*T))-1.0)**2)
  end function dbosefun

end module dynamics_conversions
