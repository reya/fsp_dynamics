module dynamics_datatypes
  implicit none 
  integer, parameter :: dp=kind(1.0d00)
  integer, parameter :: k1=selected_int_kind(2) ! 1-byte integer
  integer, parameter :: k4=selected_int_kind(8) ! 4-byte integer
  integer, parameter :: k8=selected_int_kind(10) ! 8-byte integer
  public :: dp, k1, k4, k8
  private
contains

end module dynamics_datatypes
