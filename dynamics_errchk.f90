module dynamics_errchk
  use ieee_arithmetic
  use dynamics_datatypes, only: dp
  ! ### 2016-12-09: This module should be replaced by "utils",
  !                 and now it's just a wrapper to its functions.
  use utils
  implicit none
  public :: chkmerr, chkhdferr, chknum
  public :: bohr_to_ang, ang_to_bohr
  public :: ha_to_ev, ev_to_ha, ha_to_rcm, rcm_to_ha
  public :: fcOverlapIntegral, laguerre_polynomial, fac, k1delta
  private
  
end module dynamics_errchk
