module dynamics_io
  use dynamics_datatypes
  use dynamics_rtlib
  use dynamics_errchk
  use dynamics_conversions
  implicit none
  real(kind=dp),parameter :: kB=3.16681d-6 ! Boltzmann constant in Eh/K
  public :: parseInputFile_dynamics, matrix_print
  private
  
contains

  subroutine matrix_print(A)
    real(kind=dp), intent(in) :: A(:,:)
    integer :: i,j
    write(*,"(A)") " " 
    do i=1,size(A(1,:))
       do j=1,size(A(:,1))
          write(*,"(F12.6)",advance="no") A(i,j)
       end do
       write(*,"(A)") " " 
    end do
  end subroutine matrix_print
  
  
  subroutine parseInputFile_dynamics()
    ! We read input lines in buffer and label vars.
    integer :: ios = 0, line = 0, funit=10, pos ! Note that funit=10 is arbitrary. It could be any unit you want.
    integer :: ierr
    integer :: i, j, k
    real    :: timescan
    real(kind=dp), dimension(100) :: rho_real_initial
    real(kind=dp), dimension(100) :: rho_imag_initial
    integer, dimension(100,2) :: rho_idx_initial
    integer :: max_rho_idx, idx1, idx2
    character(len=128) :: buffer, label    
    character(len=32)  :: filename

    ! ### Now we're going to re-use input parser we already have
    print *, " * Setting runtime library * "
    !
    ! ##########################
    ! ### INPUT PARSER BLOCK ###
    ! ##########################
    ! Get the input file name as a first command-line argument.
    max_rho_idx = 0
    call get_command_argument(1, filename)
    ! Open it
    open(funit, file=filename)
    ! Iterate over the keywords in the filename for the first time and get non-array parameters
    do while (ios == 0) ! while file still has lines to read
       read(funit, '(A)', iostat=ios) buffer ! we read the line and put it in the buffer
       if (ios == 0) then  ! And if ios == 0, there is a line. If it's not, file has ended.
          line = line + 1   
          pos = scan(buffer, ' ') ! find where a space first occurs
          label = buffer(1:pos)   ! split the buffer into the label, which is before a space
          buffer = buffer(pos+1:) ! and into the value, which is after the space. 
          select case (label)     ! Now let's iterate over all possible labels
          case ('sb_coupling')      ! and read the corresponding variables.
             read(buffer, *, iostat=ios) rt%sb_coupling
             !rt%sb_coupling = rcm_to_ha(rt%sb_coupling)
             print "(A,F12.6)", '  sb_coupling = ', rt%sb_coupling
          case ('T')      
             read(buffer, *, iostat=ios) rt%T
             print "(A,F12.6)", '  T           = ', rt%T
             rt%KT = rt%T * kB
             print "(A,F12.6)", '  kT           = ', rt%KT
          case ('tol_solver')      
             read(buffer, *, iostat=ios) rt%tol_solver
             print "(A,F12.6)", '  tol_solver  = ', rt%tol_solver
          case ('overlap_K')      
             read(buffer, *, iostat=ios) rt%overlap_K
             print "(A,I12)", '  overlap_K  = ', rt%overlap_K
          case ('time_start')      
             read(buffer, *, iostat=ios) rt%tstart
             print "(A,F12.6)", '  time_start  = ', rt%tstart
          case ('time_stop')      
             read(buffer, *, iostat=ios) rt%tstop
             print "(A,F18.6)", '  time_stop   = ', rt%tstop
          case ('time_step')      
             read(buffer, *, iostat=ios) rt%tstep
             print "(A,F12.6)", '  time_step   = ', rt%tstep
          case ('cutoff_freq')      
             read(buffer, *, iostat=ios) rt%cutoff_freq
             rt%cutoff_freq = rcm_to_ha(rt%cutoff_freq)
             print "(A,F12.6)", '  cutoff_freq = ', rt%cutoff_freq
          case ('use_field')      
             read(buffer, *, iostat=ios) rt%use_field
             print "(A,I6)", '  use_field = ', rt%use_field
          case ('print_rho')      
             read(buffer, *, iostat=ios) rt%print_rho
             print "(A,I6)", '  print_rho = ', rt%print_rho
          case ('field_amp_x')      
             read(buffer, *, iostat=ios) rt%field_amplitude(1)
             print "(A,F12.6)", '  field_amp_x = ', rt%field_amplitude(1)
          case ('field_amp_y')      
             read(buffer, *, iostat=ios) rt%field_amplitude(2)
             print "(A,F12.6)", '  field_amp_y = ', rt%field_amplitude(2)
          case ('field_amp_z')      
             read(buffer, *, iostat=ios) rt%field_amplitude(3)
             print "(A,F12.6)", '  field_amp_z = ', rt%field_amplitude(3)
          case ('field_start')      
             read(buffer, *, iostat=ios) rt%field_start
             print "(A,F12.6)", '  field_start = ', rt%field_start
          case ('field_fwhm')      
             read(buffer, *, iostat=ios) rt%field_fwhm
             print "(A,F12.6)", '  field_start = ', rt%field_fwhm
          case ('field_freq')      
             read(buffer, *, iostat=ios) rt%field_freq
             print "(A,F12.6)", '  field_start = ', rt%field_freq
          case ('integration')
             read(buffer, *, iostat=ios) rt%integration
             print "(A,I6)", '  integration_method = ', rt%integration
          case ('sd_ohmic')
             read(buffer, *, iostat=ios) rt%sd_ohmic
             print "(A,I6)", '  sd_ohmic = ', rt%sd_ohmic
          case ('full_secular')
             read(buffer, *, iostat=ios) rt%full_secular
             print "(A,I6)", '  full_secular = ', rt%full_secular
          case ('dense')
             read(buffer, *, iostat=ios) rt%dense
             print "(A,I6)", '  dense          = ', rt%dense
          case ('prepare_exc')
             read(buffer, *, iostat=ios) rt%prepare_exc
             print "(A,I6)", '  prepare_exc          = ', rt%prepare_exc
          case ('corr_length')
             read(buffer, *, iostat=ios) rt%correlation_length
             print "(A,F6.3)", '  correlation_length          = ', rt%correlation_length
          case ('rho_initial')
             max_rho_idx = max_rho_idx + 1
             read(buffer, *, iostat=ios) &
                  rho_idx_initial(max_rho_idx,1), rho_idx_initial(max_rho_idx,2),&
                  rho_real_initial(max_rho_idx), rho_imag_initial(max_rho_idx)
             print "(A,I4,I4,2F12.6)", '  real, imag of rho_initial at ', rho_idx_initial(max_rho_idx,1),&
                  & rho_idx_initial(max_rho_idx,2), rho_real_initial(max_rho_idx), rho_imag_initial(max_rho_idx)
          case default
             print *, "  * * * Skipping line in the input: " , line
          end select
       end if
    end do

    ! # After reading the initial DM, we set up the initial conditions
    if (rt%prepare_exc==1) then
       write(*,"(A)") " * * * TRANSITION DIPOLE MOMENTS ARE USED FOR INITIAL RHO * * *"
    else
       write(*,"(A)") " * * * INITIAL VALUES FOR RHO ARE USED * * *"
       if (max_rho_idx>0) then
          allocate(rt%rho_real_initial(max_rho_idx),stat=ierr); call chkmerr(ierr)
          allocate(rt%rho_imag_initial(max_rho_idx),stat=ierr); call chkmerr(ierr)
          allocate(rt%indicies_rho_initial(max_rho_idx,2),stat=ierr); call chkmerr(ierr)
          rt%rho_real_initial(1:max_rho_idx) = rho_real_initial(1:max_rho_idx)
          rt%rho_imag_initial(1:max_rho_idx) = rho_imag_initial(1:max_rho_idx)
          rt%indicies_rho_initial(1:max_rho_idx,:) = rho_idx_initial(1:max_rho_idx,:)
       else
          max_rho_idx=1
          allocate(rt%rho_real_initial(max_rho_idx),stat=ierr); call chkmerr(ierr)
          allocate(rt%rho_imag_initial(max_rho_idx),stat=ierr); call chkmerr(ierr)
          allocate(rt%indicies_rho_initial(max_rho_idx,2),stat=ierr); call chkmerr(ierr)
          rt%rho_real_initial(1:max_rho_idx) = 1.0
          rt%rho_imag_initial(1:max_rho_idx) = 0.0
          rt%indicies_rho_initial(1:max_rho_idx,1) = 1
          rt%indicies_rho_initial(1:max_rho_idx,2) = 1
       end if
    end if
    
    timescan = rt%tstart
    rt%time_size = 1
    do while(timescan<rt%tstop)
       rt%time_size = rt%time_size + 1
       timescan = timescan + rt%tstep
    end do
    
  end subroutine parseInputFile_dynamics

  
end module dynamics_io

