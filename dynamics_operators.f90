module dynamics_operators
  use dynamics_basis
  use dynamics_datatypes
  use dynamics_rtlib
  use dynamics_errchk
  use dynamics_conversions
  use omp_lib
  use dynamics_io, only: matrix_print
  use blas95
  use ieee_arithmetic
  implicit none 

  ! ### For now, naive dense storages without any optimizations are used ###
  ! # Real and imaginary parts of DM
  real(kind=dp), dimension(:,:), allocatable :: rho_last_real
  real(kind=dp), dimension(:,:), allocatable :: rho_last_imag

  ! # System's Hamiltonian, diagonal for now
  real(kind=dp), dimension(:,:), allocatable :: Hs
  real(kind=dp), dimension(:), allocatable :: equilibrium_population
  ! # Lambda operator matrix
  real(kind=dp), dimension(:,:), allocatable :: lambda
  ! # System operator matrix
  real(kind=dp), dimension(:,:), allocatable :: Ksystem
  real(kind=dp), dimension(:,:,:,:), allocatable :: Rabcd ! Redfield tensor
  real(kind=dp), dimension(:,:), allocatable :: Rmn ! Secular stuff
  ! #############################################
  ! ### New definitions (correct)             ###
  ! ### Definitions of q_jk, qhat_jk, and s_j ###
  ! #############################################
  ! # Operators are defined as 2D and 1D array of         #
  ! # type storing the appropriate matrix representations # 
  type(operator_representation_type), dimension(:,:), allocatable :: q_operator, qhat_operator
  type(operator_representation_type), dimension(:,:), allocatable :: qim_operator, qimhat_operator
  type(operator_representation_type), dimension(:), allocatable   :: s_operator
  integer, dimension(:,:), allocatable  ::  index_mapping_s  ! Correspondance between flat index is and (monomer, mode) pair
  ! #############################################
  ! #############################################


  ! ###################################################
  real(kind=dp), dimension(:), allocatable   :: vib_contrib
  real(kind=dp) :: beta ! 1/kT
  real(kind=dp) :: latest_time_used
  real(kind=dp), parameter :: pi=atan(1.0_dp)
  real(kind=dp), parameter   :: kB=3.16681d-6 ! Boltzmann constant in Eh/K
  integer :: number_of_operators

  logical :: isComputed_Ksystem=.FALSE., isComputed_Lambda=.FALSE.

  public :: initializeOperators
  public :: rightHandSide_full, rightHandSide_Kuhn, rightHandSide_Polyutov
  public :: rightHandSide_correct
  public :: unpackRho, packRho, packInitialRho, unpackLatestRho
  public :: rho_last_real, rho_last_imag
  public :: Ksystem, lambda
  public :: equilibrium_population
  public :: projectDM_monomer, vibronic_contribution
  private

contains

  subroutine initializeOperators()
    integer :: ierr
    integer :: i, j, k, l
    integer :: ia, ib, ic, id, ie, is, js, counter, ti, tj
    real(kind=dp) :: tstart, tstop
    real(kind=dp) :: transition_dipole_sum
    real(kind=dp), dimension(rt%space_size,rt%space_size) :: mresult
    real(kind=dp) :: t_current
    
    write(*,"(A)",advance="no") " [operators] * Allocating operators ... "  
    allocate(rho_last_real(rt%space_size,rt%space_size),stat=ierr); call chkmerr(ierr)
    allocate(rho_last_imag(rt%space_size,rt%space_size),stat=ierr); call chkmerr(ierr)
    allocate(lambda(rt%space_size,rt%space_size),stat=ierr); call chkmerr(ierr)
    allocate(Ksystem(rt%space_size,rt%space_size),stat=ierr); call chkmerr(ierr)
    ! #######################################
    ! ### Allocate q_jk, qhat_jk, and s_j ###
    ! #######################################
    ! Total number of operators: 2*Nmers*Nmodes (q on every monomer in GS and ES) + Nmers (|e><e|).
    ! First Nmers operators are electronic. Then follows Nmers*Nmodes ES vibrational operators, and Nmers*Nmodes GS vibrational operators
    number_of_operators = rt%Nmers*rt%Nmodes*2+rt%Nmers
    allocate(q_operator(number_of_operators,number_of_operators),stat=ierr); call chkmerr(ierr)
    allocate(qhat_operator(number_of_operators,number_of_operators),stat=ierr); call chkmerr(ierr)
    allocate(qim_operator(number_of_operators,number_of_operators),stat=ierr); call chkmerr(ierr)
    allocate(qimhat_operator(number_of_operators,number_of_operators),stat=ierr); call chkmerr(ierr)
    allocate(s_operator(number_of_operators),stat=ierr); call chkmerr(ierr)
    do i=1,number_of_operators
       allocate(s_operator(i)%val(rt%space_size,rt%space_size),stat=ierr); call chkmerr(ierr)
       do j=1,number_of_operators
          allocate(q_operator(i,j)%val(rt%space_size,rt%space_size),stat=ierr); call chkmerr(ierr)
          allocate(qhat_operator(i,j)%val(rt%space_size,rt%space_size),stat=ierr); call chkmerr(ierr)
          allocate(qim_operator(i,j)%val(rt%space_size,rt%space_size),stat=ierr); call chkmerr(ierr)
          allocate(qimhat_operator(i,j)%val(rt%space_size,rt%space_size),stat=ierr); call chkmerr(ierr)
       end do
    end do
    allocate(rt%sb_coupling_array(number_of_operators)); call chkmerr(ierr) ! System-bath couplings
    allocate(index_mapping_s(number_of_operators,2)); call chkmerr(ierr)     ! Mapping between flat index and (monomer, mode) pair

    write(*,*) " Done "

    
    ! ###################
    ! ### Beta = 1/kT ###
    beta = 1.0d00/(kB*rt%T)
    ! ###################
    
    ! ###################
    ! ### Latest time ###
    ! ###################
    latest_time_used = rt%tstart
    

    ! ######################################################################
    ! ### Temporary solution: set SB coupling the same for all operators ###
    rt%sb_coupling_array(:) = rt%sb_coupling                            !###
    print *, " [operators] System-bath coupling is: ", rt%sb_coupling, " Eh"
    ! ###                                                                ###
    ! ######################################################################

    ! ################################################
    ! ### Set mapping: flat to -> (monomer, mode)  ###
    counter = 0
    ! # First part: electronic only
    do is=1,rt%Nmers
       counter = counter + 1
       index_mapping_s(counter,1)=is  ! Monomer index
       index_mapping_s(counter,2)=0  ! Mode index
    end do
    ! # Second part: vibrational ES
    do is=1,rt%Nmers
       do js=1,rt%Nmodes
          counter = counter + 1
          index_mapping_s(counter,1)=is  ! Monomer index
          index_mapping_s(counter,2)=js  ! Mode index
       end do
    end do
    
    ! # Third part:  vibrational GS
    do is=1,rt%Nmers
       do js=1,rt%Nmodes
          counter = counter + 1
          index_mapping_s(counter,1)=is  ! Monomer index
          index_mapping_s(counter,2)=js  ! Mode index
       end do
    end do

    ! ################################################
    
    
    ! ################################################
    ! # First, initialize rho with initial conditions
    rho_last_real(:,:) = 0.0d00
    rho_last_imag(:,:) = 0.0d00
    if (rt%prepare_exc==1) then
       print *, " [operators] Populating density matrix according to the transition dipole from the ground state "
       transition_dipole_sum=0.0d00
       do i=1,rt%space_size
          rho_last_real(i,i) = sum(abs(transition_dipole(0,i)))
       end do
       
       !transition_dipole_sum = transition_dipole_sum + rho_last_real(i,i)
       !
       transition_dipole_sum = sum([( rho_last_real(i,i), i=1,rt%space_size )])
       do i=1,rt%space_size
          rho_last_real(i,i) = rho_last_real(i,i)/transition_dipole_sum
       end do
    else
       do i=1,ubound(rt%rho_real_initial,1)
          j = rt%indicies_rho_initial(i,1)
          k = rt%indicies_rho_initial(i,2)
          rho_last_real(j,k) = rt%rho_real_initial(i)
          rho_last_imag(j,k) = rt%rho_imag_initial(i)
       end do
    end if
    ! ###########################
    ! ### Fill the operators  ###
    ! ###########################

    ! ######################################
    ! # First: s_operator, electronic part #
    ! ######################################
    tstart = omp_get_wtime()
    write(*,"(A)",advance="no") " [operators] Computing electronic part... "
    do is=1,rt%Nmers
       ! Initialize it first
       s_operator(is)%val(:,:) = 0.0d00
       write(*,"(I4)",advance="no") is
       ! Now, start filling it's values.
       do i=1,rt%space_size
          !$OMP PARALLEL PRIVATE(j) SHARED(i,is) 
          !$OMP DO
          do j=1,rt%space_size  !
             ! Compute matrix element of s[is]=|e_{is}><e_{is}| at point i,j
             s_operator(is)%val(i,j) = compute_s_operator_electronic(is,i,j)
          end do
          !$OMP END DO
          !$OMP END PARALLEL
       end do
    end do
    write(*,"(A)") "  "

    tstop = omp_get_wtime()
    write(*,"(A,F12.6,A)") " * DONE * [",tstop-tstart," s ]" 
    ! ################################################################################
    ! # Second: s_operator for EXCITED state manifold, vibrational part at each mode #
    ! ################################################################################
    write(*,"(A)",advance="no") " [operators] Computing vibrational part (excited state)... "
    tstart = omp_get_wtime()
    do is=rt%Nmers+1,rt%Nmers*rt%Nmodes+rt%Nmers
       ! Initialize it first
       s_operator(is)%val(:,:) = 0.0d00
       write(*,"(I4)",advance="no") is
       ! Now, start filling it's values.
       do i=1,rt%space_size
          !$OMP PARALLEL PRIVATE(j) SHARED(i,is) 
          !$OMP DO
          do j=1,rt%space_size  
             ! Compute matrix element of s[is]=|e_{is}><e_{is}| at point i,j
             s_operator(is)%val(i,j) = &
                  compute_s_operator_vibrational_es(&
                  is, i, j, &
                  index_mapping_s(is,1), index_mapping_s(is,2) )
          end do
          !$OMP END DO
          !$OMP END PARALLEL
       end do
    end do
    write(*,"(A)") "  "
    tstop = omp_get_wtime()
    write(*,"(A,F12.6,A)") " * DONE * [",tstop-tstart," s ]" 

    ! ################################################################################
    ! # Third: s_operator for GROUND state manifold, vibrational part at each mode #
    ! ################################################################################
    write(*,"(A)",advance="no") " [operators] Computing vibrational part (ground state)... "
    tstart = omp_get_wtime()
    do is=rt%Nmers*rt%Nmodes+rt%Nmers+1,number_of_operators
       ! Initialize it first
       s_operator(is)%val(:,:) = 0.0d00
       write(*,"(I4)",advance="no") is
       ! Now, start filling it's values.
       do i=1,rt%space_size
          !$OMP PARALLEL PRIVATE(j) SHARED(i,is)
          !$OMP DO
          do j=1,rt%space_size  
             ! Compute matrix element of s[is]=|e_{is}><e_{is}| at point i,j
             s_operator(is)%val(i,j) = &
                  compute_s_operator_vibrational_gs(&
                  is, i, j, &
                  index_mapping_s(is,1), index_mapping_s(is,2) )
          end do
          !$OMP END DO
          !$OMP END PARALLEL
       end do
    end do
    write(*,"(A)") "  "
    tstop = omp_get_wtime()
    write(*,"(A,F12.6,A)") " * DONE * [",tstop-tstart," s ]" 
    write(*,"(A)") " "
    ! write(*,"(A)") " [operators] Purifying system operators... "
    ! counter = 0
    ! do is=1,number_of_operators
    !    counter = 0
    !    do i=1,rt%space_size
    !       do j=1,rt%space_size  
    !          if (abs(s_operator(is)%val(i,j))<1.0d-13) then 
    !             s_operator(is)%val(i,j) = 0.0d0
    !             counter = counter + 1
    !          end if
    !       end do
    !    end do
    !    if (counter>0) then
    !       write(*,"(A,I4,A)",advance="no"), " Purifying operator = ", is, " :: "
    !       write(*,"(I10,A)") counter, " values set to 0 "
    !    end if
    ! end do
    ! write(*,"(A)") "  "
    ! write(*,"(A)") " * DONE * "
    ! write(*,"(A)") " "

    ! ###############################
    ! ### System Hamiltonian part ###
    ! ###############################
    
    allocate(Hs(rt%space_size, rt%space_size),stat=ierr); call chkmerr(ierr)
    allocate(equilibrium_population(rt%space_size),stat=ierr); call chkmerr(ierr)
    Hs(:,:) = 0.0d00
    do i=1,rt%space_size
       Hs(i,i) = rt%eigenvalues(i)
       !write(*,"(A,I4,A,F10.5) ") "eigenvalues(",i,")  = ", rt%eigenvalues(i)
       equilibrium_population(i) = exp(-rt%eigenvalues(i)/(kB*rt%T))
    end do
    ! #############################################
    ! ###  Counter term: reorganization energy  ###
    ! #############################################
!     do is=1,number_of_operators !rt%Nmers*rt%Nmodes+rt%Nmers
!        call gemm(s_operator(is)%val,s_operator(is)%val,mresult)
!        Hs = Hs + (rt%sb_coupling_array(is)/2.0d00)*mresult
!     end do
    
    equilibrium_population = equilibrium_population/sum(equilibrium_population)
    write(*,"(A)") " ================================================= "
    write(*,"(A,F12.4)") "    Equilibrium populations at T = ",rt%T
    do i=1,rt%space_size
       if (equilibrium_population(i)>1.0d-5) then
          write(*,"(A,I4,A,F10.5) ") "rho_eq(",i,")  = ", equilibrium_population(i)
       else
          write(*,"(A)") " ... (smaller than 1.0d-5) "
          exit
       end if
    end do

    ! write(*,*) "  * * * TRANSITION BETWEEN EIGENSTATES * * *"
    ! write(*,*) "  Absolute valus, in cm^-1"
    ! do i=1,rt%space_size
    !    do j=i+1,rt%space_size
    !          write(*,"(I3,I3,F12.6)") i, j, abs(ha_to_rcm(rt%eigenvalues(i)-rt%eigenvalues(j)))
    !    end do
    ! end do
    ! write(*,*) "  * " 

    ! write(*,*) "  * * * VIBRONIC CONTRIBUTIONS * * *"
    ! write(*,*) "  State |   Energy, eV  |  Vibronic contribution (Nvib_i*c_i^2)"
    ! write(*,*) " "
    allocate(vib_contrib(rt%space_size))
    vib_contrib = [(vibronic_contribution(i),i=1,rt%space_size)]
    !vib_contrib = vib_contrib/rt%basis_dim ! maxval(vib_contrib)
    do i=1,rt%space_size
       if (vib_contrib(i)>1.0d-10) then
          write(*,"(A,I4,A,F10.6,A,F10.6)") "  ", i, "      ",ha_to_ev(rt%eigenvalues(i)), "     ", vib_contrib(i)
       end if
    end do
    write(*,"(A)",advance="no") " [operators] Done, moving on "  
    write(*,*) "    "

  end subroutine initializeOperators

  
  function Gamma_abcd(ia,ib,ic,id)
    real(kind=dp) :: Gamma_abcd
    integer, intent(in) :: ia, ib, ic, id
    !    Gamma_abcd = Ksystem(ia,ib)*Lambda(ic,id)
    Gamma_abcd = Ksystem(ia,ib)*Ksystem(ic,id)*&
         bath_correlation_function(&
            rt%eigenvalues(id)-rt%eigenvalues(ic))
  end function Gamma_abcd

  function intersite_correlation(site_a, site_b)
    integer, intent(in) :: site_a, site_b
    real(kind=dp) :: intersite_correlation
    integer(kind=k1) :: sa, sb
    !intersite_correlation = kdelta(site_a,site_b) !1.0d00  ! Fully correlated decoherence
    if (rt%correlation_length>0) then
       sa = index_mapping_s(site_a,1)
       sb = index_mapping_s(site_b,1)
       intersite_correlation = exp(-nrm2(rt%monomers(:,sa)-rt%monomers(:,sb))/rt%correlation_length)
    else
       intersite_correlation = kdelta(site_a,site_b)
    end if
    
  end function intersite_correlation
  
    

  ! Computes matrix element of electronic system operator
  ! between eigenstates i and j
  pure elemental function compute_s_operator_electronic(is,i,j)
    real(kind=dp) :: compute_s_operator_electronic
    integer, intent(in) :: is, i, j
    integer :: k, l, ip
    logical :: mode_flag
    logical, dimension(3) :: monomer_flags
    ! If excited and ground states are
    ! on the same monomer, and in the same vibrational state,
    ! and monomer has index "is"
    compute_s_operator_electronic = 0.0d00
    monomer_flags(:) = .TRUE.
    ! Scan the basis set and find matching pairs giving non-zero result
    do k=1,rt%basis_dim
       do l=1,rt%basis_dim
          ! it's |eA><eA|
          ! If basis functions are of the same N-particle type
          if (basis(k)%np==basis(l)%np) then
             ! If excited states are on the same monomer
             if ((basis(k)%particle(1)%idx==basis(l)%particle(1)%idx))then
                monomer_flags(1:basis(k)%np)=.FALSE.
                do ip=1,basis(k)%np
                   if (all(basis(k)%particle(ip)%vib(:)==basis(l)%particle(ip)%vib(:)))  then
                      monomer_flags(ip) = .TRUE.
                   end if
                end do
                if (all(monomer_flags)) then
                   compute_s_operator_electronic = compute_s_operator_electronic+rt%eigenvectors(k,i)*rt%eigenvectors(l,j)
                end if
             end if
          end if
       end do
    end do
    compute_s_operator_electronic = compute_s_operator_electronic * rt%sb_coupling_array(is)
  end function compute_s_operator_electronic

  ! ######################################################################################################################################
  ! ### 2017-01-27 :: It appears to me now that I don't have to consider the displacement the way I did, by adding a constant.
  !                   The reason is I take <m|q|n>, where |m> and |n> are from the same PES.
  !                   I simply have to treat position operators separatly for |g> and |e> manifolds of the monomer electronic esubsystem
  ! [SIMPLIFICATION] :: Only excited state vibrational PES is considered for coupling of the position operator to the bath.
  ! ######################################################################################################################################
  ! Computes matrix element of vibrational part of the system operator
  ! between eigenstates i and j
  pure elemental function compute_s_operator_vibrational_es(is, i, j, monomer_idx, mode_idx)
    real(kind=dp) :: compute_s_operator_vibrational_es
    integer, intent(in) :: is, i, j, monomer_idx, mode_idx
    integer :: k, l, ip, im, npA, npB
    logical :: mode_flag
    logical, dimension(3) :: monomer_flags

    compute_s_operator_vibrational_es = 0.0d00
    monomer_flags(:) = .TRUE.
    ! Scan the basis set and find matching pairs giving non-zero result
    do k=1,rt%basis_dim
       do l=1,rt%basis_dim
          if ((basis(k)%particle(1)%idx==monomer_idx).and.(basis(l)%particle(1)%idx==monomer_idx)) then
             ! If basis functions are of the same N-particle type

             if (basis(k)%np==basis(l)%np) then
                ! If excited states are on the same monomer
                if ((basis(k)%particle(1)%idx==basis(l)%particle(1)%idx)) then

                   select case (basis(k)%np)
                   case (1)
                      compute_s_operator_vibrational_es = compute_s_operator_vibrational_es+&
                           rt%eigenvectors(k,i)*rt%eigenvectors(l,j)&
                           *position_operator_ME(basis(k)%particle(1)%vib(mode_idx),basis(l)%particle(1)%vib(mode_idx)) 

                   case (2)
                      if ((basis(k)%particle(2)%idx==basis(l)%particle(2)%idx).and.&
                           (all(basis(k)%particle(2)%vib==basis(l)%particle(2)%vib))) then
                         compute_s_operator_vibrational_es = compute_s_operator_vibrational_es+&
                              rt%eigenvectors(k,i)*rt%eigenvectors(l,j)&
                              *position_operator_ME(basis(k)%particle(1)%vib(mode_idx),basis(l)%particle(1)%vib(mode_idx))
                      end if
                   case (3)
                      if ((((basis(k)%particle(2)%idx==basis(l)%particle(2)%idx).and.&
                           (all(basis(k)%particle(2)%vib==basis(l)%particle(2)%vib)))&
                           .and.&
                           ((basis(k)%particle(3)%idx==basis(l)%particle(3)%idx).and.&
                           (all(basis(k)%particle(3)%vib==basis(l)%particle(3)%vib)))&
                           )&
                           .or.&
                           (((basis(k)%particle(2)%idx==basis(l)%particle(3)%idx).and.&
                           (all(basis(k)%particle(2)%vib==basis(l)%particle(3)%vib)))&
                           .and.&
                           ((basis(k)%particle(3)%idx==basis(l)%particle(2)%idx).and.&
                           (all(basis(k)%particle(3)%vib==basis(l)%particle(2)%vib)))&
                           )&
                           )then
                         compute_s_operator_vibrational_es = compute_s_operator_vibrational_es+&
                              rt%eigenvectors(k,i)*rt%eigenvectors(l,j)&
                              *position_operator_ME(basis(k)%particle(1)%vib(mode_idx),basis(l)%particle(1)%vib(mode_idx))
                      end if
                   end select

                end if
             end if
          end if

       end do
    end do
    compute_s_operator_vibrational_es = compute_s_operator_vibrational_es * rt%sb_coupling_array(is)
  end function compute_s_operator_vibrational_es

  ! ### BAD IDEA! ###
  ! # The same body as ES, but for GS manifold #
  pure elemental function compute_s_operator_vibrational_gs(is, i, j, monomer_idx, mode_idx)
    real(kind=dp) :: compute_s_operator_vibrational_gs
    integer, intent(in) :: is, i, j, monomer_idx, mode_idx
    integer :: k, l, ip, im, np_A, np_B
    logical :: mode_flag
    logical, dimension(3) :: monomer_flags

    compute_s_operator_vibrational_gs = 0.0d00
    monomer_flags(:) = .TRUE.
    ! Scan the basis set and find matching pairs giving non-zero result
    do k=1,rt%basis_dim
       do l=1,rt%basis_dim
          ! First, if excited states are the same, np>1 for at least one of the basis functions, and monomer_idx is NOT in excited state
          if ((basis(k)%particle(1)%idx==basis(l)%particle(1)%idx).and.(all(basis(k)%particle(1)%vib(:)==basis(l)%particle(1)%vib(:))) &
               .and.(basis(k)%particle(1)%idx.ne.monomer_idx) ) then
             ! Next, need to check all 1p/2p/3p combinations.
             ! Pick up a number of cases.
             np_A = basis(k)%np
             np_B = basis(l)%np
             select case (np_A)
             case (1)
                select case(np_B)
                case (1)
                   ! # 1p-1p case: 0
                case (2)
                   ! # 1p-2p case
                   if (basis(l)%particle(2)%idx==monomer_idx) then
                      compute_s_operator_vibrational_gs = compute_s_operator_vibrational_gs + &
                           rt%eigenvectors(k,i)*rt%eigenvectors(l,j)&
                           *position_operator_ME(0,basis(l)%particle(2)%vib(mode_idx))
                   end if
                case (3)
                   ! # 1p-3p case: 0 
                case default
                   compute_s_operator_vibrational_gs = 1.0d0/0.0d0
                   return
                end select
             case (2)
                select case(np_B)
                case (1)
                   ! # 2p-1p case
                   if (basis(k)%particle(2)%idx==monomer_idx) then
                      compute_s_operator_vibrational_gs = compute_s_operator_vibrational_gs + &
                           rt%eigenvectors(k,i)*rt%eigenvectors(l,j)&
                           *position_operator_ME(basis(k)%particle(2)%vib(mode_idx),0)
                   end if
                case (2)
                   ! # 2p-2p case
                   if ((basis(k)%particle(2)%idx==monomer_idx).and.(basis(l)%particle(2)%idx==monomer_idx)) then
                      compute_s_operator_vibrational_gs = compute_s_operator_vibrational_gs + &
                           rt%eigenvectors(k,i)*rt%eigenvectors(l,j)&
                           *position_operator_ME(basis(k)%particle(2)%vib(mode_idx),basis(l)%particle(2)%vib(mode_idx))
                   end if
                case (3)
                   ! # 2p-3p case
                   if ( (basis(k)%particle(2)%idx==basis(l)%particle(3)%idx).and.&
                        all(basis(k)%particle(2)%vib(:)==basis(l)%particle(3)%vib(:)).and.&
                        basis(l)%particle(2)%idx==monomer_idx) then
                      compute_s_operator_vibrational_gs = compute_s_operator_vibrational_gs + &
                           rt%eigenvectors(k,i)*rt%eigenvectors(l,j)&
                           *position_operator_ME(0,basis(l)%particle(2)%vib(mode_idx))
                   end if
                   if ( (basis(k)%particle(2)%idx==basis(l)%particle(2)%idx).and.&
                        all(basis(k)%particle(2)%vib(:)==basis(l)%particle(2)%vib(:)).and.&
                        basis(l)%particle(3)%idx==monomer_idx) then
                      compute_s_operator_vibrational_gs = compute_s_operator_vibrational_gs + &
                           rt%eigenvectors(k,i)*rt%eigenvectors(l,j)&
                           *position_operator_ME(0,basis(l)%particle(3)%vib(mode_idx))
                   end if

                case default
                   compute_s_operator_vibrational_gs = 1.0d0/0.0d0
                   return
                end select
             case (3)
                select case(np_B)
                case (1)
                   ! # 3p-1p case: 0

                case (2)
                   ! # 3p-2p case
                   if ( (basis(l)%particle(2)%idx==basis(k)%particle(3)%idx).and.&
                        all(basis(l)%particle(2)%vib(:)==basis(k)%particle(3)%vib(:)).and.&
                        basis(k)%particle(2)%idx==monomer_idx) then
                      compute_s_operator_vibrational_gs = compute_s_operator_vibrational_gs + &
                           rt%eigenvectors(k,i)*rt%eigenvectors(l,j)&
                           *position_operator_ME(basis(k)%particle(2)%vib(mode_idx),0)
                   end if
                   if ( (basis(l)%particle(2)%idx==basis(k)%particle(2)%idx).and.&
                        all(basis(l)%particle(2)%vib(:)==basis(k)%particle(2)%vib(:)).and.&
                        basis(k)%particle(3)%idx==monomer_idx) then
                      compute_s_operator_vibrational_gs = compute_s_operator_vibrational_gs + &
                           rt%eigenvectors(k,i)*rt%eigenvectors(l,j)&
                           *position_operator_ME(basis(k)%particle(3)%vib(mode_idx),0)
                   end if

                case (3)
                   ! # 3p-3p case
                   if ( (basis(k)%particle(2)%idx==monomer_idx).and.(basis(l)%particle(2)%idx==monomer_idx)&
                        .and.(basis(k)%particle(3)%idx==basis(l)%particle(3)%idx).and.&
                        (all(basis(k)%particle(3)%vib(:)==basis(l)%particle(3)%vib(:))) ) then
                      compute_s_operator_vibrational_gs = compute_s_operator_vibrational_gs + &
                           rt%eigenvectors(k,i)*rt%eigenvectors(l,j)&
                           *position_operator_ME(basis(k)%particle(2)%vib(mode_idx),basis(l)%particle(2)%vib(mode_idx))
                   end if
                   if ( (basis(k)%particle(2)%idx==monomer_idx).and.(basis(l)%particle(3)%idx==monomer_idx)&
                        .and.(basis(k)%particle(3)%idx==basis(l)%particle(2)%idx).and.&
                        (all(basis(k)%particle(3)%vib(:)==basis(l)%particle(2)%vib(:))) ) then
                      compute_s_operator_vibrational_gs = compute_s_operator_vibrational_gs + &
                           rt%eigenvectors(k,i)*rt%eigenvectors(l,j)&
                           *position_operator_ME(basis(k)%particle(2)%vib(mode_idx),basis(l)%particle(3)%vib(mode_idx))
                      
                   end if
                case default
                   compute_s_operator_vibrational_gs = 1.0d0/0.0d0
                   return
                end select
             case default ! # This should never happen. If it does, create a floating-point exception, because it's a pure function
                compute_s_operator_vibrational_gs = 1.0d0/0.0d0
                return
             end select
          end if
       end do
    end do
    compute_s_operator_vibrational_gs = compute_s_operator_vibrational_gs * rt%sb_coupling_array(is)
  end function compute_s_operator_vibrational_gs
  
  
  subroutine rightHandSide_correct(t,rho_in,drhodt)
    use blas95
    use f95_precision
    integer, parameter :: dp_kind=kind(1.0d00)
    real(kind=dp_kind), intent(in) :: t
    real(kind=dp_kind), intent(in), dimension(2*rt%space_size**2) :: rho_in
    real(kind=dp_kind), intent(out), dimension(2*rt%space_size**2) :: drhodt
    real(kind=dp_kind), dimension(rt%space_size,rt%space_size) :: rho_real, drhodt_real
    real(kind=dp_kind), dimension(rt%space_size,rt%space_size) :: rho_imag, drhodt_imag, mresult, mresult2, Hfield
    integer :: i, j, k, counter, is, js

    call unpackRho(rho_in,rho_real,rho_imag)
    ! # Initialize output
    drhodt_real(:,:) = 0.0d00
    drhodt_imag(:,:) = 0.0d00
    drhodt(:) = 0.0d00

    ! ###############################
    ! ###  q and qhat operators   ###
    ! ###############################

    if (t.ne.latest_time_used) then
       do is=1,number_of_operators
          do js=1,number_of_operators
             ! Initialize q and qhat operators
             q_operator(is,js)%val(:,:) = 0.0d00
             qhat_operator(is,js)%val(:,:) = 0.0d00
             qim_operator(is,js)%val(:,:) = 0.0d00
             qimhat_operator(is,js)%val(:,:) = 0.0d00
             do i=1,rt%space_size
                !$OMP PARALLEL PRIVATE(j) 
                !$OMP DO
                do j=1,rt%space_size
                   q_operator(is,js)%val(i,j) = 0.5d00*s_operator(js)%val(i,j)*&
                        intersite_correlation(is,js)*&
                        rt%sb_coupling_array(js)*bath_correlation_function_ex(rt%eigenvalues(j)-rt%eigenvalues(i),t)
                   qhat_operator(is,js)%val(i,j) = 0.5d00*s_operator(js)%val(i,j)*&
                        intersite_correlation(is,js)*&
                        rt%sb_coupling_array(js)*bath_correlation_function_ex(rt%eigenvalues(i)-rt%eigenvalues(j),t)
                   qim_operator(is,js)%val(i,j) = 0.5d00*s_operator(js)%val(i,j)*&      ! No conjugation
                        intersite_correlation(is,js)*&
                        rt%sb_coupling_array(js)*bath_correlation_function_imaginary_ex(rt%eigenvalues(j)-rt%eigenvalues(i),t)
                   qimhat_operator(is,js)%val(i,j) =-0.5d00*s_operator(js)%val(i,j)*&  ! Complex conjugated part
                        intersite_correlation(is,js)*&
                        rt%sb_coupling_array(js)*bath_correlation_function_imaginary_ex(rt%eigenvalues(i)-rt%eigenvalues(j),t)
                end do
                !$OMP END DO
                !$OMP END PARALLEL
             end do
          end do
       end do
       latest_time_used = t
       rt%expensive_steps = rt%expensive_steps + 1
    end if
        
    
    ! ### First: compute real part ###
    ! # H*rho - rho*H
    call gemm(-rho_imag,Hs,mresult)
    drhodt_real = mresult
    call gemm(Hs,-rho_imag,mresult)
    drhodt_real = drhodt_real - mresult
    do j=1,number_of_operators
       do k=1,number_of_operators
          ! #  + q_jk*rho*s_j #
          call gemm(rho_real,s_operator(j)%val,mresult)
          call gemm(q_operator(j,k)%val,mresult,mresult2)
          drhodt_real = drhodt_real + mresult2
          ! #  - s_j*q_jk*rho #
          call gemm(q_operator(j,k)%val,rho_real,mresult)
          call gemm(s_operator(j)%val,mresult,mresult2)
          drhodt_real = drhodt_real - mresult2
          ! #  + s_j*rho*qhat_jk
          call gemm(rho_real,qhat_operator(j,k)%val,mresult)
          call gemm(s_operator(j)%val,mresult,mresult2)
          drhodt_real = drhodt_real + mresult2
          ! #  - rho*qhat_jk*s_j
          call gemm(qhat_operator(j,k)%val,s_operator(j)%val,mresult)
          call gemm(rho_real,mresult,mresult2)
          drhodt_real = drhodt_real - mresult2

          ! ### COHERENT PART: LAMB SHIFT ###
          ! #  - q(I)_jk*rho(I)*s_j #
          call gemm(rho_imag,s_operator(j)%val,mresult)
          call gemm(qim_operator(j,k)%val,mresult,mresult2)
          drhodt_real = drhodt_real - mresult2
          ! #  + s_j*q(I)_jk*rho(I) #
          call gemm(qim_operator(j,k)%val,rho_imag,mresult)
          call gemm(s_operator(j)%val,mresult,mresult2)
          drhodt_real = drhodt_real + mresult2
          ! #  - s_j*rho(I)*qhat(I)_jk #
          call gemm(rho_imag,qimhat_operator(j,k)%val,mresult)
          call gemm(s_operator(j)%val,mresult,mresult2)
          drhodt_real = drhodt_real - mresult2
          ! #  + rho(I)*qhat(I)_jk*s_j#
          call gemm(qimhat_operator(j,k)%val,s_operator(j)%val,mresult)
          call gemm(rho_imag,mresult,mresult2)
          drhodt_real = drhodt_real + mresult2
          ! ###           END             ###
       end do
    end do
       
    ! ### Second: compute imaginary part ###
    ! # H*rho - rho*H
    call gemm(rho_real,Hs,mresult)
    drhodt_imag = mresult
    call gemm(Hs,rho_real,mresult)
    drhodt_imag = drhodt_imag - mresult
    do j=1,number_of_operators
       do k=1,number_of_operators
          ! #  + q_jk*rho*s_j #
          call gemm(rho_imag,s_operator(j)%val,mresult)
          call gemm(q_operator(j,k)%val,mresult,mresult2)
          drhodt_imag = drhodt_imag + mresult2
          ! #  - s_j*q_jk*rho #
          call gemm(q_operator(j,k)%val,rho_imag,mresult)
          call gemm(s_operator(j)%val,mresult,mresult2)
          drhodt_imag = drhodt_imag - mresult2
          ! #  + s_j*rho*qhat_jk
          call gemm(rho_imag,qhat_operator(j,k)%val,mresult)
          call gemm(s_operator(j)%val,mresult,mresult2)
          drhodt_imag = drhodt_imag + mresult2
          ! #  - rho*qhat_jk*s_j
          call gemm(qhat_operator(j,k)%val,s_operator(j)%val,mresult)
          call gemm(rho_imag,mresult,mresult2)
          drhodt_imag = drhodt_imag - mresult2

          ! ### COHERENT PART: LAMB SHIFT ###
          ! #  + q_jk*rho*s_j #
          call gemm(rho_real,s_operator(j)%val,mresult)
          call gemm(qim_operator(j,k)%val,mresult,mresult2)
          drhodt_imag = drhodt_imag + mresult2
          ! #  - s_j*q_jk*rho #
          call gemm(qim_operator(j,k)%val,rho_real,mresult)
          call gemm(s_operator(j)%val,mresult,mresult2)
          drhodt_imag = drhodt_imag - mresult2
          ! #  + s_j*rho*qhat_jk
          call gemm(rho_real,qimhat_operator(j,k)%val,mresult)
          call gemm(s_operator(j)%val,mresult,mresult2)
          drhodt_imag = drhodt_imag + mresult2
          ! #  - rho*qhat_jk*s_j
          call gemm(qimhat_operator(j,k)%val,s_operator(j)%val,mresult)
          call gemm(rho_real,mresult,mresult2)
          drhodt_imag = drhodt_imag - mresult2
       end do
    end do

    ! # Numerical adequacy check #

    do i=1,rt%space_size
       !$OMP PARALLEL PRIVATE(j) SHARED(i)
       !$OMP DO 
       do j=1,rt%space_size
          call chknum(drhodt_real(i,j),info="drho/dt Real")
          call chknum(drhodt_imag(i,j),info="drho/dt Imag")
       end do
       !$OMP END DO
       !$OMP END PARALLEL
    end do
    ! ### Finally: pack the derivative and return it ###
    call packRho(drhodt,drhodt_real,drhodt_imag)
  end subroutine rightHandSide_correct
  
  subroutine rightHandSide_full(t,rho_in,drhodt)
    use blas95
    use f95_precision
    integer, parameter :: dp_kind=kind(1.0d00)
    real(kind=dp_kind), intent(in) :: t
    real(kind=dp_kind), intent(in), dimension(2*rt%space_size**2) :: rho_in
    real(kind=dp_kind), intent(out), dimension(2*rt%space_size**2) :: drhodt
    real(kind=dp_kind), dimension(rt%space_size,rt%space_size) :: rho_real, drhodt_real
    real(kind=dp_kind), dimension(rt%space_size,rt%space_size) :: rho_imag, drhodt_imag, mresult, mresult2, Hfield
    integer :: i, j, k, counter

    call unpackRho(rho_in,rho_real,rho_imag)
    ! # Initialize output
    drhodt_real(:,:) = 0.0d00
    drhodt_imag(:,:) = 0.0d00
    drhodt(:) = 0.0d00
    ! ### First: compute real part ###
    ! # H*rho - rho*H
    call gemm(Hs,rho_imag,mresult)
    drhodt_real = mresult
    call gemm(rho_imag,Hs,mresult)
    drhodt_real = drhodt_real - mresult
    ! # Lambda*rho*K - K*rho*Lambda
    call gemm(rho_real,Ksystem,mresult)
    call gemm(lambda,mresult,mresult2)
    drhodt_real = drhodt_real + mresult2
    call gemm(lambda,rho_real,mresult)
    call gemm(Ksystem,mresult,mresult2)
    drhodt_real = drhodt_real - mresult2
    ! # K*rho*Lambda' - rho*Lambda'*K
    call gemm(rho_real,lambda,mresult,transa='N',transb='T')
    call gemm(Ksystem,mresult,mresult2)
    drhodt_real = drhodt_real + mresult2
    call gemm(lambda,Ksystem,mresult,transa='T',transb='N')
    call gemm(rho_real,mresult,mresult2)
    drhodt_real = drhodt_real - mresult2

    ! ### Second: compute imaginary part ###
    ! # H*rho - rho*H
    call gemm(rho_real,Hs,mresult)
    drhodt_imag = mresult
    call gemm(Hs,rho_real,mresult)
    drhodt_imag = drhodt_imag - mresult
    ! # Lambda*rho*K - K*rho*Lambda
    call gemm(rho_imag,Ksystem,mresult)
    call gemm(lambda,mresult,mresult2)
    drhodt_imag = drhodt_imag + mresult2
    call gemm(lambda,rho_imag,mresult)
    call gemm(Ksystem,mresult,mresult2)
    drhodt_imag = drhodt_imag - mresult2
    ! # K*rho*Lambda' - rho*Lambda'*K
    call gemm(rho_imag,lambda,mresult,transa='N',transb='T')
    call gemm(Ksystem,mresult,mresult2)
    drhodt_imag = drhodt_imag + mresult2
    call gemm(lambda,Ksystem,mresult,transa='T',transb='N')
    call gemm(rho_imag,mresult,mresult2)
    drhodt_imag = drhodt_imag - mresult2
    ! Test: zero out imaginary part of diagonal elements to avoid noise accumulation
    do i=1,rt%space_size
       drhodt_imag(i,i) = 0.0d00
    end do
    ! ### Finally: pack the derivative and return it ###
    call packRho(drhodt,drhodt_real,drhodt_imag)
  end subroutine rightHandSide_full

  subroutine rightHandSide_Kuhn(t,rho_in,drhodt)
    use blas95
    use f95_precision
    integer, parameter :: dp_kind=kind(1.0d00)
    real(kind=dp_kind), intent(in) :: t
    real(kind=dp_kind), intent(in), dimension(2*rt%space_size**2) :: rho_in
    real(kind=dp_kind), intent(out), dimension(2*rt%space_size**2) :: drhodt
    real(kind=dp_kind), dimension(rt%space_size,rt%space_size) :: rho_real, drhodt_real
    real(kind=dp_kind), dimension(rt%space_size,rt%space_size) :: rho_imag, drhodt_imag, mresult, mresult2
    real(kind=dp_kind) :: sum_dissipation_real, sum_dissipation_imag
    integer :: i, j, k, counter, ic, id
    
    call unpackRho(rho_in,rho_real,rho_imag)
    ! # Initialize output
    drhodt_real(:,:) = 0.0d00
    drhodt_imag(:,:) = 0.0d00
    drhodt(:) = 0.0d00
    ! ### First: compute real part ###
    ! # H*rho - rho*H
    call gemm(Hs,rho_imag,mresult)
    drhodt_real = mresult
    call gemm(rho_imag,Hs,mresult)
    drhodt_real = drhodt_real - mresult

    ! ! # Lambda*rho*K - K*rho*Lambda
    ! call gemm(rho_real,Ksystem,mresult)
    ! call gemm(lambda,mresult,mresult2)
    ! drhodt_real = drhodt_real + mresult2
    ! call gemm(lambda,rho_real,mresult)
    ! call gemm(Ksystem,mresult,mresult2)
    ! drhodt_real = drhodt_real - mresult2
    ! ! # K*rho*Lambda' - rho*Lambda'*K
    ! call gemm(rho_real,lambda,mresult,transa='N',transb='T')
    ! call gemm(Ksystem,mresult,mresult2)
    ! drhodt_real = drhodt_real + mresult2
    ! call gemm(lambda,Ksystem,mresult,transa='T',transb='N')
    ! call gemm(rho_real,mresult,mresult2)
    ! drhodt_real = drhodt_real - mresult2

    ! ### Second: compute imaginary part ###
    ! # H*rho - rho*H
    call gemm(rho_real,Hs,mresult)
    drhodt_imag = mresult
    call gemm(Hs,rho_real,mresult)
    drhodt_imag = drhodt_imag - mresult
    ! ! # Lambda*rho*K - K*rho*Lambda
    ! call gemm(rho_imag,Ksystem,mresult)
    ! call gemm(lambda,mresult,mresult2)
    ! drhodt_imag = drhodt_imag + mresult2
    ! call gemm(lambda,rho_imag,mresult)
    ! call gemm(Ksystem,mresult,mresult2)
    ! drhodt_imag = drhodt_imag - mresult2
    ! ! # K*rho*Lambda' - rho*Lambda'*K
    ! call gemm(rho_imag,lambda,mresult,transa='N',transb='T')
    ! call gemm(Ksystem,mresult,mresult2)
    ! drhodt_imag = drhodt_imag + mresult2
    ! call gemm(lambda,Ksystem,mresult,transa='T',transb='N')
    ! call gemm(rho_imag,mresult,mresult2)
    ! drhodt_imag = drhodt_imag - mresult2

    do i=1,rt%space_size
       do j=1,rt%space_size
          sum_dissipation_real = 0.0d00
          sum_dissipation_imag = 0.0d00
          do ic=1,rt%space_size
             do id=1,rt%space_size
                sum_dissipation_real = sum_dissipation_real + Rabcd(i,j,ic,id)*rho_real(ic,id)
                sum_dissipation_imag = sum_dissipation_imag + Rabcd(i,j,ic,id)*rho_imag(ic,id)
             end do
          end do
          drhodt_real(i,j) = drhodt_real(i,j) - sum_dissipation_real
          drhodt_imag(i,j) = drhodt_imag(i,j) - sum_dissipation_imag
       end do
    end do
    
    ! ### Finally: pack the derivative and return it ###
    call packRho(drhodt,drhodt_real,drhodt_imag)
  end subroutine rightHandSide_Kuhn

  subroutine rightHandSide_Polyutov(t,rho_in,drhodt)
    use blas95
    use f95_precision
    integer, parameter :: dp_kind=kind(1.0d00)
    real(kind=dp_kind), intent(in) :: t
    real(kind=dp_kind), intent(in), dimension(2*rt%space_size**2) :: rho_in
    real(kind=dp_kind), intent(out), dimension(2*rt%space_size**2) :: drhodt
    real(kind=dp_kind), dimension(rt%space_size,rt%space_size) :: rho_real, drhodt_real
    real(kind=dp_kind), dimension(rt%space_size,rt%space_size) :: rho_imag, drhodt_imag, mresult, mresult2
    real(kind=dp_kind) :: sum_dissipation_real, sum_dissipation_imag, sum_dissipation
    real(kind=dp_kind), dimension(rt%space_size) :: rho_real_list
    integer :: i, j, k, counter, ic, id
    integer :: ia, ib
    
    call unpackRho(rho_in,rho_real,rho_imag)

    ! # Initialize output
    drhodt_real(:,:) = 0.0d00
    drhodt_imag(:,:) = 0.0d00
    drhodt(:) = 0.0d00

    ! call gemm(Hs,rho_imag,mresult)
    ! drhodt_real = mresult
    ! call gemm(rho_imag,Hs,mresult)
    ! drhodt_real = drhodt_real - mresult
    ! call gemm(rho_real,Hs,mresult)
    ! drhodt_imag = mresult
    ! call gemm(Hs,rho_real,mresult)
    ! drhodt_imag = drhodt_imag - mresult
    do ia=1,rt%space_size
       rho_real_list(ia) = rho_real(ia,ia)
    end do
    rho_real_list(:) = rho_real_list(:)/maxval(rho_real_list)
    
    do ia=1,rt%space_size
       sum_dissipation = 0.0d00
       do ib=1,rt%space_size
          sum_dissipation = sum_dissipation + Rmn(ia,ib)*rho_real_list(ib)
       end do
       drhodt_real(ia,ia) = -1.0d00*sum_dissipation
    end do
    
    ! ### Finally: pack the derivative and return it ###
    call packRho(drhodt,drhodt_real,drhodt_imag)
  end subroutine rightHandSide_Polyutov

  
  subroutine unpackRho(rho_in, rho_real, rho_imag)
    real(kind=dp), intent(in), dimension(2*rt%space_size**2) :: rho_in
    real(kind=dp), intent(out), dimension(rt%space_size,rt%space_size) :: rho_real
    real(kind=dp), intent(out), dimension(rt%space_size,rt%space_size) :: rho_imag
    integer :: j, k, counter
    counter = 0
    do j=1,rt%space_size
       do k=1,rt%space_size
          counter = counter + 1
          rho_real(j,k) = rho_in(counter)
          rho_imag(j,k) = rho_in(counter+rt%space_size**2)
       end do
    end do
  end subroutine unpackRho

  
  subroutine packRho(rho_out, rho_real, rho_imag)
    real(kind=dp), intent(out), dimension(2*rt%space_size**2) :: rho_out
    real(kind=dp), intent(in), dimension(rt%space_size,rt%space_size) :: rho_real
    real(kind=dp), intent(in), dimension(rt%space_size,rt%space_size) :: rho_imag
    integer :: j, k, counter
    counter = 0

    do j=1,rt%space_size
       do k=1,rt%space_size
          counter = counter + 1
          rho_out(counter) = rho_real(j,k)
          rho_out(counter+rt%space_size**2) = rho_imag(j,k)
       end do
    end do
  end subroutine packRho

  subroutine packInitialRho(rho_out)
    real(kind=dp), intent(out), dimension(2*rt%space_size**2) :: rho_out
    call packRho(rho_out,rho_last_real,rho_last_imag)
  end subroutine packInitialRho

  subroutine unpackLatestRho(rho_in)
    real(kind=dp), intent(in), dimension(2*rt%space_size**2) :: rho_in
    call unpackRho(rho_in,rho_last_real, rho_last_imag)
  end subroutine unpackLatestRho
  
  
  ! # Function compute Ksystem matrix between eigenstates i and j
  function computeKsystem(i,j)
    integer, intent(in) :: i, j
    integer :: k, l
    real(kind=dp) :: computeKsystem
    real(kind=dp) :: tmp, tmp2
    
    computeKsystem = 0.0d00
    if (rt%overlap_K==0) then
       do k=1,rt%basis_dim
          do l=1,rt%basis_dim
             tmp = positionOperatorME(k,l)
             tmp2 = rt%eigenvectors(k,i)*rt%eigenvectors(l,j)
             computeKsystem = computeKsystem + tmp2 * tmp
          end do
       end do
    else if (rt%overlap_K==2) then
       do k=1,rt%basis_dim
          computeKsystem = computeKsystem + &
               abs(rt%eigenvectors(k,i)*rt%eigenvectors(k,j))
       end do
       do k=1,rt%basis_dim
          do l=1,rt%basis_dim
             tmp = positionOperatorME(k,l)
             tmp2 = rt%eigenvectors(k,i)*rt%eigenvectors(l,j)
             computeKsystem = computeKsystem + tmp2 * tmp
          end do
       end do
    else if (rt%overlap_K==1) then
       do k=1,rt%basis_dim
          computeKsystem = computeKsystem + &
               abs(rt%eigenvectors(k,i)*rt%eigenvectors(k,j))
       end do
    else
       write(*,*) " Wrong overlap_K"
       stop
    end if

  end function computeKsystem
  
  ! # Function compute Lambda matrix between eigenstates i and j
  function computeLambda(i,j)
    integer, intent(in) :: i, j
    real(kind=dp) :: computeLambda
    computeLambda = 0.0d00
    ! if (((rt%eigenvalues(i)-rt%eigenvalues(j)).ne.0.0d00)) then
!       write(*,"(A,I2,I2,A,F12.6,A,F12.6)") &
 !           " i, j = ", i, j, " Ksystem = ",Ksystem(i,j), " ; omega = ",&
!            rt%eigenvalues(i)-rt%eigenvalues(j)
    computeLambda = Ksystem(i,j)*bath_correlation_function(&
            rt%eigenvalues(j)-rt%eigenvalues(i)) ! # Important: i<->j changes things
!       write(*,*) "   Lambda = ", Ksystem(i,j)*bath_correlation_function(&
!            rt%eigenvalues(i)-rt%eigenvalues(j))
!    end if
    
  end function computeLambda

  function bath_correlation_function(omega)
    real(kind=dp), intent(in) :: omega
    real(kind=dp) :: bath_correlation_function
    !# Ignoring chemical potential: why?
    ! if (omega==0.0d00)  then
    !    bath_correlation_function = !0.0d00!2.0d00*pi*rt%sb_coupling*rt%KT ! # For Ohmic bath: limit omega*Exp(-omega)*BE(omega)=1
    ! else
    if (rt%sd_ohmic==0) then
       if (omega==0.0d00) then
          bath_correlation_function = kB*rt%T
       else
          bath_correlation_function = (1.0d00+1.0d00/(Exp(omega/(kB*rt%T))-1))*&
               (spectral_density_ohmic(omega)) !-spectral_density_ohmic(-omega))
       end if
    else if (rt%sd_ohmic==1) then ! 1 for Debye
       if (omega==0.0d00) then
          bath_correlation_function = kB*rt%T/rt%cutoff_freq !2*kB*rt%T
       else 
          bath_correlation_function = (1.0d00+1.0d00/(Exp(omega/(kB*rt%T))-1))*&
               spectral_density_debye(omega)
       end if
    else if (rt%sd_ohmic==2) then ! 2 for Pullerits
       if (omega==0.0d00) then
          bath_correlation_function = 0 !2*kB*rt%T ! Wrong limit(!) 0
       else 
          bath_correlation_function = (1.0d00+1.0d00/(Exp(omega/(kB*rt%T))-1))*&
               spectral_density_pullerits(omega)
       end if
    end if
  end function bath_correlation_function

  ! #############################################################
  ! ### Defined only for Ohmic type with Drude-Lorentz cutoff ###
  ! #############################################################
  function bath_correlation_function_ex(omega,t)
    real(kind=dp), intent(in) :: omega, t
    real(kind=dp) :: bath_correlation_function_ex
    real(kind=dp) :: matsubara_term, nu_k, term1, term2
    integer :: k
    bath_correlation_function_ex = 0.0d00
    ! ### First part ###
    !  Put omega0 = 1, and thus eta is defined in terms of 1/omega0.
    bath_correlation_function_ex = (1.0d00/(2.0d00*(rt%cutoff_freq**2+omega**2)))*&
         exp(-rt%cutoff_freq*t)*rt%cutoff_freq * &
         (&
         exp(rt%cutoff_freq*t)*omega - omega*cos(omega*t) - rt%cutoff_freq*sin(omega*t) &
         + (1.0d00/tan(beta*rt%cutoff_freq/2.0d00)) * (&
         exp(rt%cutoff_freq*t)*rt%cutoff_freq - &
         rt%cutoff_freq*cos(omega*t) + omega*sin(omega*t)&
         )&
         )

    ! ### Matsubara sum ###
    k = 1
    matsubara_term = 1.0d00
    do while (abs(matsubara_term)>1.0d-10) ! Arbitrary convergence criteria
       if (k>1000) then
          exit
       end if
       
       nu_k = matsubara_freq(k)
       matsubara_term = 2.0d00*exp(-nu_k*t)*rt%cutoff_freq*nu_k*&
            (exp(nu_k*t)*nu_k-nu_k*cos(omega*t)+omega*sin(omega*t))/&
            (beta*(-(rt%cutoff_freq**2)+nu_k**2)*(nu_k**2+omega**2))
            ! (nu_k**2/(nu_k**2+rt%cutoff_freq**2))/&
            ! (beta*(-(rt%cutoff_freq**2)+nu_k**2)*(nu_k**2+omega**2))
       if (ieee_is_normal(matsubara_term)) then
          if (abs(matsubara_term)>1.0d-15) then
             bath_correlation_function_ex = bath_correlation_function_ex + matsubara_term
          end if
       else
!          write(*,*) " Nope: ", matsubara_term
          matsubara_term = 0.0d0
       end if
       k = k + 1
    end do
    call chknum(bath_correlation_function_ex,info="Bath correlation function: real.")
   !write(*,*) " Output from bath cf = ", bath_correlation_function_ex, " at t = ", t, " omega = ", omega
  end function bath_correlation_function_ex

  ! #############################################################
  ! ### Defined only for Ohmic type with Drude-Lorentz cutoff ###
  ! #############################################################
  function bath_correlation_function_imaginary_ex(omega,t)
    real(kind=dp), intent(in) :: omega, t
    real(kind=dp) :: bath_correlation_function_imaginary_ex
    real(kind=dp) :: matsubara_term, nu_k, term1, term2
    integer :: k
    bath_correlation_function_imaginary_ex = 0.0d00
    ! ### First part ###
    !  Put omega0 = 1, and thus eta is defined in terms of 1/omega0.
    bath_correlation_function_imaginary_ex = (-1.0d00/(2.0d00*(rt%cutoff_freq**2+omega**2)))*&
         exp(-rt%cutoff_freq*t)*rt%cutoff_freq*&
         (&
         exp(rt%cutoff_freq*t)*rt%cutoff_freq - rt%cutoff_freq*cos(omega*t)+omega*sin(omega*t)&
         +(1.0d00/tan(beta*rt%cutoff_freq/2.0d00))*&
         (&
         -exp(rt%cutoff_freq*t)*omega+omega*cos(omega*t)+rt%cutoff_freq*sin(omega*t)&
         )&
         )

    ! ### Matsubara sum ###
    k = 1
    matsubara_term = 1.0d00
    do while (abs(matsubara_term)>1.0d-10) ! Arbitrary convergence criteria
       if (k>1000) then
          exit
       end if
       
       nu_k = matsubara_freq(k)
       matsubara_term = -2.0d00*exp(-nu_k*t)*rt%cutoff_freq*nu_k*&
            (-exp(nu_k*t)*omega+omega*cos(omega*t)+nu_k*sin(omega*t))/&
            (beta*(-(rt%cutoff_freq**2)+nu_k**2)*(nu_k**2+omega**2))
!            (nu_k**2/(nu_k**2+rt%cutoff_freq**2))/&
 !           (beta*(-(rt%cutoff_freq**2)+nu_k**2)*(nu_k**2+omega**2))
       if (ieee_is_normal(matsubara_term)) then
          if (abs(matsubara_term)>1.0d-15) then
             bath_correlation_function_imaginary_ex = &
                  bath_correlation_function_imaginary_ex + matsubara_term
          end if
          
       else
          ! write(*,*) " Nope: ", matsubara_term
          ! write(*,*) " 1:    ", -2.0d00*exp(-nu_k*t)*rt%cutoff_freq*nu_k, exp(-nu_k*t)
          ! write(*,*) " 2:    ", (-exp(nu_k*t)*omega+omega*cos(omega*t)+nu_k*sin(omega*t)), exp(nu_k*t)
          ! write(*,*) " 3:    ",             (beta*(-(rt%cutoff_freq**2)+nu_k**2)*(nu_k**2+omega**2))
          matsubara_term = 0.0d00
       end if
       k = k + 1
    end do
    call chknum(bath_correlation_function_imaginary_ex,info="Bath correlation function: imaginary.")
  end function bath_correlation_function_imaginary_ex


  pure function matsubara_freq(k)
    integer, intent(in) :: k
    real(kind=dp) :: matsubara_freq
    matsubara_freq = 2.0*pi*k/beta
  end function matsubara_freq
  
  
  function bath_correlation_function_imaginary(omega)
    real(kind=dp), intent(in) :: omega
    real(kind=dp) :: bath_correlation_function_imaginary
    integer(kind=4)  :: number_of_evals, ierr
    real(kind=dp) :: output_result, absolute_error
    integer(kind=4) :: last
    integer(kind=4), dimension(15000) :: iwork
    real(kind=dp), dimension(4*15000) :: work
    !# Ignoring chemical potential: why?
    ! if (omega==0.0d00)  then
    !    bath_correlation_function = !0.0d00!2.0d00*pi*rt%sb_coupling*rt%KT ! # For Ohmic bath: limit omega*Exp(-omega)*BE(omega)=1
    ! else
    iwork(:) = 0
    work(:) = 0.0d00
    write(*,*) " [dynamics_operators] ERROR: calling a deprecated func&
         &tion."
    stop
    ! call dqawc( bath_correlation_function, &
    !      -10.0d00, &
    !      10.0d00, omega, 1.0D-10, 1.0D-10, &
    !      output_result, absolute_error, number_of_evals, ierr, &
    !      15000,4*15000,last,iwork,work)
    if (ierr.ne.0) then
       write(*,"(A,I3)") "Stopping: failed to take integral, ierr = ", ierr
       write(*,*) " abserr = ", absolute_error
       write(*,*) " neval = ", number_of_evals
       write(*,*) " omega = ", omega
       stop
    end if
    bath_correlation_function_imaginary = -(1.0d00/pi) * output_result 
!    write(*,*) " omega = ", omega, " : ", bath_correlation_function_imaginary, " err = ", absolute_error
    

  end function bath_correlation_function_imaginary


   ! pure elemental function spectral_density_constant(omega)
  !   real(kind=dp), intent(in) :: omega
  !   real(kind=dp) :: spectral_density_constant
  !   if (omega>0.0d00) then
  !      spectral_density_constant = 1
  !   else if (omega<0.0d00) then
  !      spectral_density_constant = exp(omega/(kB*rt%T))
  !   end if
  ! end function spectral_density_constant
  
  pure elemental function spectral_density_pullerits(omega)
    real(kind=dp), intent(in) :: omega
    real(kind=dp) :: spectral_density_pullerits
    if (omega>0) then
       spectral_density_pullerits = (omega**2) * exp(-omega/rt%cutoff_freq) / &
            (2*rt%cutoff_freq**3)
    else if (omega<0) then
       spectral_density_pullerits = -(omega**2) * exp(omega/rt%cutoff_freq) / &
       (2*rt%cutoff_freq**3)
    end if
  end function spectral_density_pullerits

  pure elemental function spectral_density_ohmic(omega)
    real(kind=dp), intent(in) :: omega
    real(kind=dp) :: spectral_density_ohmic
    if (omega>0.0d00) then
       spectral_density_ohmic = omega * exp(-omega/rt%cutoff_freq)
    else if (omega<0.0d00) then
       spectral_density_ohmic = omega * exp(omega/rt%cutoff_freq)
    end if
  end function spectral_density_ohmic

  ! pure elemental function spectral_density_coth(omega)
  !   real(kind=dp), intent(in) :: omega
  !   real(kind=dp) :: spectral_density_coth
  !   if (omega==0.0d00) then
  !      spectral_density_coth = rt%sb_coupling*rt%kT
  !   else
  !      spectral_density_coth = rt%sb_coupling * (omega) * coth(omega/rt%kT)
  !   end if
  ! end function spectral_density_coth

  pure elemental function spectral_density_debye(omega)
    real(kind=dp), intent(in) :: omega
    real(kind=dp) :: spectral_density_debye
    spectral_density_debye=0.0d00
    if (omega>0.0d00) then
       spectral_density_debye = (omega * rt%cutoff_freq)/(omega**2 + rt%cutoff_freq**2)
    else if (omega<0.0d00) then
       spectral_density_debye = (omega * rt%cutoff_freq)/(omega**2 + rt%cutoff_freq**2)
    end if
  end function spectral_density_debye

  
  integer pure function kdelta(i,j)
    integer(kind=k1), intent(in) :: i,j
    if (i==j) then
       kdelta = 1
    else
       kdelta = 0
    end if
  end function kdelta

  ! ### BROKEN :: Computed position operator between different eigenstates ###
  function positionOperatorME(k,l)
    integer, intent(in) :: k,l
    integer :: i, j, im, M, N
    real(kind=dp) :: positionOperatorME
    positionOperatorME = 0.0d00
    print *, " [operators] ERROR: Using a borken function. Exiting."
    stop
    ! do im=1,rt%Nmodes
    !    ! # If ground states are on the same monomer
    !    if (basis_set(k)%basis(im)%gidx==basis_set(l)%basis(im)%gidx) then
    !       ! # If excited states are on the same monomer
    !       if (basis_set(k)%basis(im)%eidx==basis_set(l)%basis(im)%eidx) then
    !          ! ### Check if ME is non-zero
    !          ! # if |gAk-gAl|==1 and eBk==eBl
    !          if (abs(basis_set(k)%basis(im)%g-basis_set(l)%basis(im)%g)==1) then
    !             if (basis_set(k)%basis(im)%e==basis_set(l)%basis(im)%e) then
    !                M=basis_set(k)%basis(im)%g
    !                N=basis_set(l)%basis(im)%g
    !                positionOperatorME = positionOperatorME + &
    !                     (1.0d00/sqrt(2.0d00))*&
    !                     (kdelta(M+1,N)*sqrt(M+1.0d0)+kdelta(M-1,N)*sqrt(1.0d00*M))
    !             end if
    !          end if
    !          ! # if |eAk-eAl|==1 and gBk==gBl
    !          if (abs(basis_set(k)%basis(im)%e-basis_set(l)%basis(im)%e)==1) then
    !             if (basis_set(k)%basis(im)%g==basis_set(l)%basis(im)%g) then
    !                M=basis_set(k)%basis(im)%e
    !                N=basis_set(l)%basis(im)%e
    !                positionOperatorME = positionOperatorME + &
    !                     (1.0d00/sqrt(2.0d00))*&
    !                     (kdelta(M+1,N)*sqrt(M+1.0d0)+kdelta(M-1,N)*sqrt(1.0d00*M))
    !             end if
    !          end if
    !       end if
    !    end if
    ! end do

  end function positionOperatorME

  function electric_field(t)
    real(kind=dp), dimension(3) :: electric_field
    real(kind=dp), intent(in) :: t
    electric_field(:) = rt%field_amplitude(:)*&
         exp(-(t-rt%field_start)/(2.0d00*rt%field_fwhm**2))*&
         cos(rt%field_freq*t)&
         /(sqrt(2.0*pi)*rt%field_fwhm)
  end function electric_field

  !! Transition dipole moments between ground state g and excitonic state e
  !! For now, only g=0 is supported
  function transition_dipole(g,e)
    integer, intent(in) :: g, e
    real(kind=dp), dimension(3) :: transition_dipole
    integer :: i, im
    integer :: e_idx
    real(kind=dp) :: fc_prod
    if (g.ne.0) then
       write(*,*) " [ERROR] Transition dipole moment is supported only from g=0"
       stop
    end if
    transition_dipole(:) = 0.0d0
    do i=1,rt%basis_dim
       if (basis(i)%np==1) then
          fc_prod = 1.0d00
          do im=1,rt%Nmodes
             e_idx    = basis(i)%particle(1)%vib(im)
             fc_prod  = fc_prod*fcOverlapIntegral(0,e_idx,rt%HRs(im))
          end do
          transition_dipole = transition_dipole + &
               rt%eigenvectors(i,e)*fc_prod*rt%dipoles(:,basis(i)%particle(1)%idx)
       end if
    end do
    
  end function transition_dipole

  ! ##############################################################
  ! # INPUT: real part of density matrix rho_in                  #
  ! # OUTPUT: populations projected on the monomer_idx subspace  #
  ! ##############################################################
  real(kind=dp) function projectDM_monomer(monomer_idx,rho_in)
    integer, intent(in) :: monomer_idx
    real(kind=dp), dimension(rt%space_size,rt%space_size) :: rho_in
    real(kind=dp) :: pro
    integer :: i, im, j, k, n, istate, ip

    projectDM_monomer = 0.0d00
    

    do istate=1,rt%space_size
       do i=1,rt%basis_dim
          if (basis(i)%particle(1)%idx==monomer_idx) then
             projectDM_monomer = projectDM_monomer + rt%eigenvectors(i,istate)**2*rho_in(istate,istate)
          end if
       end do
    end do
    
    ! # Scan every basis function
    ! do i=1,rt%basis_dim
    !    ! # Scan excitonic states
    !    do istate=1,rt%space_size
    !       if (basis_set(i)%basis(1)%eidx==monomer_idx) then
    !          ! # CASE 1: basis function has monomer with the same index in excited state
    !          ! # Scan every vibrational excitation in every mode in the monomer
    !          do n=0,rt%Ne-1
    !             do im=1,rt%Nmodes
    !                if (basis_set(i)%basis(im)%e==n) then
    !                   projectDM_monomer = projectDM_monomer &
    !                        + rt%eigenvectors(i,istate)**2*rho_in(istate,istate)
    !                end if
    !             end do
    !          end do
    !       else if (basis_set(i)%basis(1)%gidx==monomer_idx) then
    !          ! # CASE 2: basis function has monomer with the same index in ground state
    !          ! # Scan every vibrational excitation in every mode in the monomer
    !          do n=0,rt%Ne-1
    !             do im=1,rt%Nmodes
    !                projectDM_monomer = projectDM_monomer &
    !                     + rt%eigenvectors(i,istate)**2*rho_in(istate,istate)*&
    !                     fcOverlapIntegral(basis_set(i)%basis(im)%g,n,rt%HRs(im))**2
    !             end do
    !          end do
    !       end if
    !    end do
    ! end do
  end function projectDM_monomer

  pure real(kind=dp) function vibronic_contribution(state_in)
    integer, intent(in) :: state_in
    integer :: i, im, vibquanta
    vibronic_contribution = 0.0d00
    ! do i=1,rt%basis_dim
    !    vibquanta = 0
    !    do im=1,rt%Nmodes
    !       if (basis_set(i)%basis(im)%e>0) then
    !          vibquanta = vibquanta + basis_set(i)%basis(im)%e
    !          !vibquanta = 1
    !       end if
    !       if (basis_set(i)%basis(im)%g>0) then
    !          vibquanta = vibquanta + basis_set(i)%basis(im)%g
    !          !vibquanta = 1
    !       end if
    !    end do
    !    if (vibquanta>0) then
    !       vibronic_contribution = vibronic_contribution + &
    !            (rt%eigenvectors(i,state_in)**2)*vibquanta
    !    end if
    ! end do
  end function vibronic_contribution

  ! Computes <k|q|m> for k,m
  pure elemental function position_operator_ME(k,m)
    real(kind=dp) :: position_operator_ME
    integer(kind=k1), intent(in) :: k, m
    position_operator_ME = 0.0d0
    if (abs(k-m)==1) then
       position_operator_ME = (1.0d00/sqrt(2.0d00))*&
            (kdelta(k,m+1_k1)*sqrt(m+1.0d00)+kdelta(k,m-1_k1)*sqrt(m*1.0d00))
    end if
  end function position_operator_ME
  
end module dynamics_operators
