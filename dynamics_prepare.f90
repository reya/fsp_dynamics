module dynamics_prepare
  use dynamics_basis
  use dynamics_operators
!  use dynamics_propagation
  use dynamics_rtlib
  use dynamics_io
  implicit none

  public :: prepareDynamics
  private
contains


  ! # Initializes everything needed
  subroutine prepareDynamics()
    ! ##############
    ! ### Step   ### 
    ! ##############
    ! # MODULE: dynamics_rtlib #
    ! #
    ! # Read job input from input.h5
    
    call readJobData()
    
    call parseInputFile_dynamics()
    ! ##############
    ! ### Step   ### 
    ! ##############
    ! # MODULE: dynamics_basis #
    call initialize_basis_set_v2(basis_size=rt%basis_dim, compute=.FALSE., print_output=.FALSE., Ng=rt%Ng, Ne=rt%Ne, Nmodes=rt%Nmodes, Nmers=rt%Nmers, Nparticle=rt%Nparticle)

    print *, " *** Basis dimension: ", rt%basis_dim
    print *, "     Now computing basis set components"
    call initialize_basis_set_v2(basis_size=rt%basis_dim, compute=.TRUE., print_output=.FALSE., Ng=rt%Ng, Ne=rt%Ne, Nmodes=rt%Nmodes, Nmers=rt%Nmers, Nparticle=rt%Nparticle)
        
    ! ##############
    ! ### Step   ###
    ! ##############
    ! # MODULE: dynamics_operators #
    ! #
    ! # Prepare operators for Redfield dynamics
    
    call initializeOperators()
    write(*,*) " [prepare] Ready for propagation"
  end subroutine prepareDynamics
  
end module dynamics_prepare
