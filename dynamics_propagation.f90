module dynamics_propagation
  use dynamics_datatypes
  use dynamics_rtlib
  use dynamics_operators
  use dynamics_conversions
  use omp_lib
  use io
  implicit none

  logical :: solverCN=.FALSE.
  real(kind=dp) :: time_step_CN, time_step_CN_Y0
  real(kind=dp), dimension(:), allocatable :: Y0, Y_current, Fcurrent
  integer :: NSTEPS_CN
  real(kind=dp) :: RMSE_CN
  real(kind=dp), allocatable, dimension(:) :: timeline
  logical :: previous_time_computed
  public :: propagateDM, propagateDM_CN, time_step_CN, time_step_CN_Y0, Y0, NSTEPS_CN, RMSE_CN, previous_time_computed
  public :: Fcurrent
  private

contains
  
  subroutine propagateDM()
    use iso_c_binding
    real(kind=dp), dimension(1) :: RPAR=0.0d00
    real(kind=dp), dimension(rt%Nmers,1) :: population_monomers
    integer(kind=C_LONG), dimension(1)       :: IPAR=0
    real(kind=dp) :: dt, t, tout, cpu_tstart, cpu_tstop, wf_norm, wf_norm_total
    real(kind=dp) :: total_tstop, total_tstart
    integer  :: i, j, k, step_number
    integer  :: ierr, tcount
    integer(kind=C_LONG), dimension(21) :: IOUT
    logical :: problematic_step
    real(kind=dp), dimension(6) :: ROUT
    character(17) :: rho_name
    character(20) :: monomers_name
    real(kind=dp) :: tol_solver

    allocate(Y0(psize))
    allocate(Y_current(psize))
    ! # Initialize NVECTOR module (serial)
    call FNVINITS(1,psize,ierr); call chkfcv(ierr)
    !call FNVINITOMP(1,psize,4,ierr); call chkfcv(ierr)

    ! # Unpack the wavefunctions into a vector
    call packInitialRho(Y0)
    tol_solver=rt%tol_solver
    ! # Allocate CVODE internals
    call FCVMALLOC(rt%tstart, Y0, 1, & ! 2 = BDF, 1 = Adams
         1, & ! 1 = functional iteration, 2 = Newton iteration
         1, & ! 1 = scalar absolute tolerance (?), 2 = array absolute tolerance (?), 3 = user-defined
         tol_solver, &! Relative tolerance
         tol_solver, &! Absolute tolerance
         IOUT, ROUT, & ! Integer and real optional outputs
         IPAR, RPAR, & ! Integer and real optional parameters
         ierr); call chkfcv(ierr)

    ! # Set diagonal Jacobian approximation
    call FCVDIAG(ierr); call chkfcv(ierr)

    ! # Use SPGMR solver
    if (rt%dense==1) then
       call FCVDENSE(psize,ierr); call chkfcv(ierr)
    else
       call FCVSPGMR(0,& ! 0 = for no preconditioner (problematic!)
            1, & ! Modified GS orthogonalization
            10000, & ! Maximum size of Krylov subspace
            tol_solver, & ! Tolerance
            ierr); call chkfcv(ierr)
    end if
    
    
    !call FCVSETIIN('MAX_ORD',2,ierr)
    call FCVSETIIN('MAX_NSTEPS',20000,ierr)
    
    ! #############################
    ! # Prepare to run the solver #
    ! #############################
    dt = rt%tstep
    t  = rt%tstart
    write (*,*) " **************************** "
    write (*,*) " * Starting the propagation * "
    write (*,*) " **************************** "
    write (*,"(A,F10.5,A,F15.2)") " From tstart = ", rt%tstart, " to tstop = ", rt%tstop
    write (*,"(A,F10.5,A)") " Solutions will be saved at dt = ", rt%tstep, " intervals"
    ! # Save initial data
    !    call writePlainDataset(t, y0, psize)
    call unpackLatestRho(y0)

    step_number = 0
    write(rho_name,"(A7,I10.10)") "/rho_re",step_number
    call writeHDF5dataset2D_new(rho_last_real,[rt%space_size, rt%space_size],rho_name,"DMpropagation.h5")
    write(rho_name,"(A7,I10.10)") "/rho_im",step_number
    call writeHDF5dataset2D(rho_last_imag,[rt%space_size, rt%space_size],rho_name,"DMpropagation.h5")
    call writeHDF5dataset2D(Ksystem,[rt%space_size, rt%space_size],"/Ksystem","DMpropagation.h5")
    call writeHDF5dataset2D(lambda,[rt%space_size, rt%space_size],"/Lambda","DMpropagation.h5")
    call writeHDF5dataset2D(equilibrium_population,[rt%space_size, 1],"/rho_equilibrium","DMpropagation.h5")

    do i=1,rt%Nmers
       population_monomers(i,1) = projectDM_monomer(i,rho_last_real)
    end do
    population_monomers(:,1) = population_monomers(:,1)/sum(population_monomers(:,1))
    write(monomers_name,"(A10,I10.10)") "/monomers_",step_number
    call writeHDF5dataset2D(population_monomers,[rt%Nmers,1],monomers_name,"DMpropagation.h5")

    tcount=1
    t=rt%tstart
    do while (t<=rt%tstop)
       tcount = tcount + 1
       t = t+rt%tstep
    end do
    allocate(timeline(tcount+1))
    timeline(1) = rt%tstart
    tcount=1
    
    t=rt%tstart
    ! # Solve the system of equations
    step_number = 0
    rt%number_of_steps = 0
    rt%expensive_steps = 0
    print *, " [propagation] NumThreads = ", omp_get_num_threads()
    total_tstart = omp_get_wtime()
    do while (t<=rt%tstop)
       ! # Since solution at the initial time is known,  
       ! # we're stepping forward and ask for the solution
       ! # at time step t + dt
       t = t + dt
       tcount = tcount + 1
       timeline(tcount) = t
       step_number = step_number + 1
       !       cpu_tstart = omp_get_wtime()

       cpu_tstart = omp_get_wtime()
       call FCVODE(t, tout, y_current, &
            1,  & ! 1 = overshoot and interpolate to t,  2 = one-step mode
            ierr);

!       call omp_get_wtime(cpu_tstop)
       cpu_tstop = omp_get_wtime()

       call unpackLatestRho(y_current)
       write(rho_name,"(A7,I10.10)") "/rho_re",step_number
       call writeHDF5dataset2D(rho_last_real,[rt%space_size, rt%space_size],rho_name,"DMpropagation.h5")
       write(rho_name,"(A7,I10.10)") "/rho_im",step_number
       call writeHDF5dataset2D(rho_last_imag,[rt%space_size, rt%space_size],rho_name,"DMpropagation.h5")

       write(*,"(A,F10.4,A,F16.6,A,I6,A,I6,A,F12.8)") "[",cpu_tstop-cpu_tstart," s]  Time: ", t, " NSTEPS = ",rt%number_of_steps,&
            " EXSTEPS = ", rt%expensive_steps, " Trace = ",trace(rho_last_real)
       if (abs(trace(rho_last_real)-1.0d00)>1.0d-10) then
          write(*,*) "ERROR: trace problem ",abs(trace(rho_last_real)-1.0d00)
          stop
       end if
       
       write(*,"(A)") , "   * real(rho_ii): "
       problematic_step = .FALSE.
       do i=1,rt%space_size
          write(*,"(A,I4,A,F15.12,A,F10.7)") "     * ",i," :: ", rho_last_real(i,i), "  ", abs(rho_last_real(i,i) - equilibrium_population(i))
          if ((rho_last_real(i,i)<-1.0d-3).or.(rho_last_real(i,i)>(1.0d00+1.0d-3))) then
             problematic_step = .TRUE.
          end if
       end do
       
       do i=1,rt%Nmers
          population_monomers(i,1) = projectDM_monomer(i,rho_last_real)
       end do
       population_monomers(:,1) = population_monomers(:,1)/sum(population_monomers(:,1))
       write(monomers_name,"(A10,I10.10)") "/monomers_",step_number
       call writeHDF5dataset2D(population_monomers,[rt%Nmers,1],monomers_name,"DMpropagation.h5")

       if (problematic_step) then
          write(*,*) " ERROR: Elements are out of physical region"
          stop
       end if
       
       if (ierr<0) then
          write(*,*) " ERROR HAS OCCURED DURING PROPAGATION: ierr = ", ierr
          write(*,*) " * EXITING * "
          stop
       end if
       call FCVREINIT(t,y_current,1,tol_solver,tol_solver,ierr); call chkfcv(ierr)
    end do

    call writeHDF5dataset2D(timeline,[tcount,1],"/t","DMpropagation.h5")
    total_tstop = omp_get_wtime()
    write(*,"(A,F16.2,A)") " PROPAGATION FINISHED IN ",total_tstop-total_tstart, " SECONDS"

  end subroutine propagateDM

  subroutine propagateDM_CN()
    use iso_c_binding
    real(kind=dp), dimension(psize) :: USCALE,FSCALE,U
    real(kind=dp), dimension(rt%Nmers,1) :: population_monomers
    integer :: i, j, k, ierr, globalstrat
    integer(kind=C_LONG),dimension(15) :: IOUT
    real(kind=dp), dimension(2) :: ROUT
    real(kind=dp) :: cpu_tstart, cpu_tstop, total_tstart, total_tstop
    real(kind=dp) :: t
    integer :: step_number, tcount
    character(17) :: rho_name
    character(20) :: monomers_name
    logical :: problematic_step

    allocate(Y0(psize))
    allocate(Y_current(psize))
    allocate(Fcurrent(psize))
    Fcurrent(:) = 0.0d00
    write(*,"(A)") "Trying CN solver"
    USCALE(:)=1.0d00; FSCALE(:)=1.0d00
    Y0(:) = 0.0d00
    call FNVINITS(3,psize,ierr); call chkfcv(ierr)
    !call FNVINITOMP(3,psize,omp_get_num_threads(),ierr); call chkfcv(ierr)
    call FKINMALLOC(IOUT,ROUT,IERR); call chkfcv(ierr)
    globalstrat=1
    call fkinsetrin('FNORM_TOL', 1.0d-15, ierr); call chkfcv(ierr)
    call fkinsetrin('SSTEP_TOL', 1.0d-15, ierr); call chkfcv(ierr)
    if (rt%dense==1) then
       call fkindense(psize, ierr)
    else
       call fkinspgmr(10000, 50, ierr); call chkfcv(ierr)
    end if
    
    call fkinsetiin('MAX_NITERS', 1000, ierr); call chkfcv(ierr)
    call fkinsetiin('MAX_SETUPS', 1000, ierr); call chkfcv(ierr)


    write (*,*) " **************************** "
    write (*,*) " * Starting the propagation * "
    write (*,*) " **************************** "
    write (*,"(A,F10.5,A,F15.2)") " From tstart = ", rt%tstart, " to tstop = ", rt%tstop
    write (*,"(A,F10.5,A)") " Solutions will be saved at dt = ", rt%tstep, " intervals"
    ! # Save initial data
    call packInitialRho(Y0)
    
    !call unpackLatestRho(Y0)

    step_number = 0
    write(rho_name,"(A7,I10.10)") "/rho_re",step_number
    call writeHDF5dataset2D_new(rho_last_real,[rt%space_size, rt%space_size],rho_name,"DMpropagation.h5")
    write(rho_name,"(A7,I10.10)") "/rho_im",step_number
    call writeHDF5dataset2D(rho_last_imag,[rt%space_size, rt%space_size],rho_name,"DMpropagation.h5")
    call writeHDF5dataset2D(Ksystem,[rt%space_size, rt%space_size],"/Ksystem","DMpropagation.h5")
    call writeHDF5dataset2D(lambda,[rt%space_size, rt%space_size],"/Lambda","DMpropagation.h5")
    call writeHDF5dataset2D(equilibrium_population,[rt%space_size, 1],"/rho_equilibrium","DMpropagation.h5")

    
    do i=1,rt%Nmers
       population_monomers(i,1) = projectDM_monomer(i,rho_last_real)
    end do
    population_monomers(:,1) = population_monomers(:,1)/sum(population_monomers(:,1))
    write(monomers_name,"(A10,I10.10)") "/monomers_",step_number
    call writeHDF5dataset2D(population_monomers,[rt%Nmers,1],monomers_name,"DMpropagation.h5")
!    call writeHDF5dataset2D_new(population_monomers,[rt%space_size, rt%space_size],rho_name,"DMpropagation.h5")
!    stop
    tcount=1
    t=rt%tstart
    do while (t<=rt%tstop)
       tcount = tcount + 1
       t = t + rt%tstep
    end do
    allocate(timeline(tcount+1))

    t = rt%tstart
    timeline(1) = rt%tstart
    tcount=1

    ! # Solve the system of equations
    step_number = 0

        
    ! # Since solution at the initial time is known,  
    ! # we're stepping forward and ask for the solution
    ! # at time step t + dt

    time_step_CN = rt%tstart
    rt%number_of_steps = 0
    rt%expensive_steps = 0
    total_tstart = omp_get_wtime()
    do while (time_step_CN<=rt%tstop)
       time_step_CN_Y0 = time_step_CN
       time_step_CN = time_step_CN_Y0 + rt%tstep
       step_number = step_number + 1
       y_current = y0
       
       tcount = tcount + 1
       timeline(tcount) = time_step_CN


       cpu_tstart = omp_get_wtime()
       NSTEPS_CN = 0
       RMSE_CN = 0.0d00

       previous_time_computed = .FALSE.
       Fcurrent(:) = 0.0d00
       call fkinsol(y_current,GLOBALSTRAT, USCALE, FSCALE, ierr);! call chkfcv(ierr)
       cpu_tstop = omp_get_wtime()
       call unpackLatestRho(y_current)
       y0=y_current
       
       write(rho_name,"(A7,I10.10)") "/rho_re",step_number
       call writeHDF5dataset2D(rho_last_real,[rt%space_size, rt%space_size],rho_name,"DMpropagation.h5")
       write(rho_name,"(A7,I10.10)") "/rho_im",step_number
       call writeHDF5dataset2D(rho_last_imag,[rt%space_size, rt%space_size],rho_name,"DMpropagation.h5")
       write(*,"(A,F10.4,A,F16.6,A,F12.8,A,I3,A,I6,A,I6,A,F15.12)") "[",cpu_tstop-cpu_tstart," s]  Time: (fs) ", time_au_to_fs(time_step_CN), " Trace = "&
            &,trace(rho_last_real), "  NSTEPS_CN=", NSTEPS_CN, "  NSTEPS = ",rt%number_of_steps, " EXSTEPS = ", &
            rt%expensive_steps, " RMSE = ", RMSE_CN
       if (abs(trace(rho_last_real)-1.0d00)>1.0d-10) then
          write(*,*) "ERROR: trace problem. trace(rho)-1 = ",abs(trace(rho_last_real)-1.0d00)
          stop
       end if
       
       do i=1,rt%Nmers
          population_monomers(i,1) = projectDM_monomer(i,rho_last_real)
       end do
       population_monomers(:,1) = population_monomers(:,1)/sum(population_monomers(:,1))
       write(monomers_name,"(A10,I10.10)") "/monomers_",step_number
       call writeHDF5dataset2D(population_monomers,[rt%Nmers,1],monomers_name,"DMpropagation.h5")

       if (rt%print_rho==1) then
          write(*,"(A)") , "   * real(rho_ii): "
       end if
       
       problematic_step = .FALSE.
       do i=1,rt%space_size
          if (rt%print_rho==1) then
             write(*,"(A,I4,A,F15.12)") "     * ",i," :: ", rho_last_real(i,i)
          end if
          if ((rho_last_real(i,i)<-1.0d-5).or.(rho_last_real(i,i)>(1.0d00+1.0d-5))) then
             problematic_step = .TRUE.
          end if
       end do

       if (problematic_step) then
          write(*,*) " ERROR: Elements are out of physical region"
          stop
       end if
       
       if (ierr<0) then
          write(*,*) " ERROR HAS OCCURED DURING PROPAGATION: ierr = ", ierr
          write(*,*) " * EXITING * "
          call writeHDF5dataset2D(timeline,[tcount,1],"/t","DMpropagation.h5")
          stop
       end if
       
    end do
    call writeHDF5dataset2D(timeline,[tcount,1],"/t","DMpropagation.h5")
    total_tstop = omp_get_wtime()
    write(*,"(A,F16.2,A)") " PROPAGATION FINISHED IN ",total_tstop-total_tstart, " SECONDS"
  end subroutine propagateDM_CN

  
  subroutine chkfcv(ierr)
    integer, intent(in) :: ierr
    if (ierr.ne.0) then
       write(*,*) "ERROR with FCV: exiting with ierr = ", ierr
       stop
    end if
  end subroutine chkfcv

  function trace(mat_in)
    real(kind=dp), dimension(:,:) :: mat_in
    integer :: i
    integer,dimension(2) :: dims
    real(kind=dp) :: trace
    trace = 0.0d00
    dims = ubound(mat_in)
    do i=1,dims(1)
       trace = trace + mat_in(i,i)
    end do
  end function trace
  
end module dynamics_propagation
