module dynamics_rtlib
  use hdf5
  use h5lt
  use dynamics_errchk
  use dynamics_conversions
  use dynamics_datatypes
  use io
!  use utils

  implicit none

  type particle_type
     integer(kind=k1) :: idx                 ! Index of monomer
     integer(kind=k1), allocatable :: vib(:) ! Vibrational excitations are here
  end type particle_type

  ! basis is an array of that type
  type basis_function_type
     type(particle_type), allocatable :: particle(:) ! N-particle components
     integer(kind=k1)                 :: np          ! N-particle number
  end type basis_function_type
  
  type rtlib_type
     integer, allocatable :: Ng(:), Ne(:)
     integer :: space_size, basis_dim, Nmodes, Nmers, Nparticle
     real(kind=dp), allocatable :: monomers(:,:)
     real(kind=dp), allocatable :: dipoles(:,:)
     real(kind=dp), allocatable :: modes(:)
     real(kind=dp), allocatable :: HRs(:)
     real(kind=dp), allocatable :: eigenvectors(:,:)
     real(kind=dp), allocatable :: eigenvalues(:)
     real(kind=dp) :: T, KT
     real(kind=dp) :: sb_coupling
     real(kind=dp) :: correlation_length
     real(kind=dp), allocatable :: sb_coupling_array(:) ! Storage for system-bath couplings (electronic and vibrational)
     real(kind=dp) :: cutoff_freq
     real(kind=dp) :: tstart, tstop, tstep, tol_solver
     real(kind=dp), allocatable :: rho_real_initial(:)
     real(kind=dp), allocatable :: rho_imag_initial(:)
     real(kind=dp), dimension(3) :: field_amplitude
     real(kind=dp)              :: field_freq
     real(kind=dp)              :: field_start, field_fwhm
     integer, allocatable       :: indicies_rho_initial(:,:)
     integer                    :: number_of_steps, time_size
     integer                    :: expensive_steps
     integer                    :: use_field
     integer                    :: print_rho
     integer                    :: prepare_exc  ! # 1 for inital rho = after pulse
                                                ! # 0 otherwise
     integer                    :: integration  ! # 1 BDF Formula with Newton step
                                                ! # 2 CN integrator with GMRES
     integer                    :: dense     ! # 0 for sparse solver,
                                             ! # 1 for dense LAPACK-based
     integer                    :: overlap_K ! # 0 for exact K, 1 for overlap-based
     integer                    :: sd_ohmic
     integer                    :: full_secular ! # 0 for exact, 1 for full secular approximation
  end type rtlib_type
  
  ! ############################################
  ! # Type to store 2nd rank array as a        #
  ! # representation of a particular operator  #
  ! ############################################
  type operator_representation_type
     real(kind=dp), allocatable :: val(:,:)
  end type operator_representation_type
  ! ############################################

  type(rtlib_type) :: rt

  logical :: rtlib_initialized=.FALSE.
  integer :: psize
!  public :: readHDF5dataset_size
!  public :: readHDF5dataset2D, writeHDF5dataset2D, writeHDF5dataset2D_new
!  public :: writeHDF5dataset1D
  public :: readJobData
  public :: datasetExists
  public :: rt
  public :: psize
  !public :: fcOverlapIntegral
  public :: operator_representation_type
  public :: basis_function_type
  private

contains


  subroutine readJobData()
    integer, dimension(1,3) :: ngemodes
    integer :: ierr, i, j, k
    integer, dimension(2) :: size_out
    real(kind=dp), allocatable, dimension(:,:) :: arr_temp
    real(kind=dp) :: errsum

    if (rtlib_initialized) then
       write(*,*) "ERROR in dynamics_rtlib: Trying to initalize rtlib twice; Exiting."
       stop
    end if
    
    call readHDF5int(rt%Nmodes,"/Nmodes","input.h5")
    write(*,"(A,I4)") "    Nmodes = ", rt%Nmodes
    allocate(rt%Ng(rt%Nmodes),stat=ierr); call chkmerr(ierr,info="rt%Ng")
    call readHDF5dataset1D_INT(data=rt%Ng,size_in=rt%Nmodes,location="/Ng",filename="input.h5")
    write(*,"(A,10I4)") "    Ng     = ", rt%Ng
    allocate(rt%Ne(rt%Nmodes),stat=ierr); call chkmerr(ierr,info="rt%Ne")
    call readHDF5dataset1D_INT(data=rt%Ne,size_in=rt%Nmodes,location="/Ne",filename="input.h5")
    write(*,"(A,10I4)") "    Ne     = ", rt%Ne
    call readHDF5int(rt%Nmers,"/Nmers",filename="input.h5")
    write(*,"(A,I4)") "    Nmers  = ", rt%Nmers
    call readHDF5int(rt%Nparticle,"/Nparticle",filename="input.h5")
    write(*,"(A,I4)") "    Nparticle  = ", rt%Nparticle
    
    allocate(rt%monomers(3,rt%Nmers),stat=ierr); call chkmerr(ierr);
    allocate(rt%dipoles(3,rt%Nmers),stat=ierr); call chkmerr(ierr);
    call readHDF5dataset_size(size_out,"/positions","input.h5")
    allocate(arr_temp(size_out(1),size_out(2)),stat=ierr); call chkmerr(ierr)
    call readHDF5dataset2D(arr_temp,size_out,"/positions","input.h5")
    arr_temp = ang_to_bohr(arr_temp)
    do i=1,rt%Nmers
       rt%monomers(:,i) = arr_temp(:,i)
    end do

    call readHDF5dataset2D(arr_temp,size_out,"/magnitude","input.h5")
    do i=1,rt%Nmers
       rt%dipoles(:,i) = arr_temp(:,i)
    end do

    write(*,"(A)") " * Initializing monomers * "
    do i=1,rt%Nmers

       write(*,"(A,I3,A,3F12.6)") "   Monomer at (x,y,z)/Bohr",i," : ",rt%monomers(:,i)
       write(*,"(A,I3,A,3F12.6)") "   Dipole (dx,dy,dz)/a.u. ",i," : ",rt%dipoles(:,i)
    end do
    write(*,"(A)") " * Done with monomers * "
    write(*,"(A)") "                        "

    write(*,"(A)") " * Vibrational modes and HR factors * "
    allocate(rt%modes(rt%Nmodes),stat=ierr); call chkmerr(ierr);
    allocate(rt%HRs(rt%Nmodes),stat=ierr); call chkmerr(ierr);
    call readHDF5dataset1D(rt%modes,rt%Nmodes,"/modes","input.h5")
    call readHDF5dataset1D(rt%HRs,rt%Nmodes,"/HRs","input.h5")
    rt%modes = rcm_to_ha(rt%modes)
    do i=1,rt%Nmodes
       write(*,"(A,F12.6,A,F12.6)") "    Freq/a.u. = ", rt%modes(i), "  HR = ", rt%HRs(i)
    end do
    write(*,"(A)")     "  "
    write(*,"(A)") " * Done with modes and HRs * "
    write(*,"(A)")     "  "

    write(*,"(A)") " * Reading eigenvalues and eigenvectors * "

    call readHDF5dataset_size(size_out(1),"/w","eigenstates.h5")
    allocate(rt%eigenvalues(size_out(1)),stat=ierr); call chkmerr(ierr)
    rt%space_size=size_out(1)
    
    write(*,"(A,I8)") "   Size of the subspace: ", rt%space_size
    call readHDF5dataset1D(rt%eigenvalues,rt%space_size,"/w","eigenstates.h5")


    call readHDF5dataset_size(size_out,"/U","eigenstates.h5")
    rt%basis_dim=size_out(1)

    allocate(rt%eigenvectors(size_out(1),size_out(2)),stat=ierr); call chkmerr(ierr)

    call readHDF5dataset2D(rt%eigenvectors,size_out,"/U","eigenstates.h5")
    write(*,*) "   Checking eigenpairs "

    do i=1,rt%space_size
       write(*,"(A,I8,A,F10.5)") "      E(",i,") = ",rt%eigenvalues(i)
       !       write(*,"(A,I8,A)", advance="no") "      U(:,",i,") = "
       !       do j=1,rt%basis_dim
       !          write(*,"(F8.4)",advance="no") rt%eigenvectors(j,i)
       !       end do
       !       write(*,"(A)") " "
    end do

    psize = 2*rt%space_size**2

  end subroutine readJobData


  ! ###
  ! #########################################################
  ! ###
  ! subroutine readHDF5dataset_size(size_out,location,filename,halt,problem)
  !   integer, intent(out), dimension(2) :: size_out
  !   integer(HSIZE_T), dimension(2) :: dims
  !   integer(HID_T) :: file_id
  !   integer(SIZE_T) :: i, j
  !   integer :: type_class
  !   integer(SIZE_T) :: type_size
  !   character(*), intent(in) :: location, filename
  !   integer :: hdferr
  !   logical, intent(out), optional :: problem
  !   logical, intent(in), optional :: halt

  !   call h5open_f(hdferr); call chkhdferr(hdferr);
  !   call H5FOpen_f(filename, H5F_ACC_RDONLY_F, file_id, hdferr); call chkhdferr(hdferr);
  !   call h5ltget_dataset_info_f(file_id, location, dims, type_class, type_size, hdferr); call chkhdferr(hdferr);
  !   size_out = dims
  !   call H5FClose_f(file_id,hdferr); call chkhdferr(hdferr);
  !   call h5close_f(hdferr); call chkhdferr(hdferr);
  ! end subroutine readHDF5dataset_size

  subroutine datasetExists(dataset_exists, location, filename)
    character(*), intent(in) :: location, filename
    logical, intent(out) :: dataset_exists
    integer(HID_T) :: file_id
    integer :: hdferr

    call h5open_f(hdferr); call chkhdferr(hdferr);
    call H5FOpen_f(filename, H5F_ACC_RDONLY_F, file_id, hdferr); call chkhdferr(hdferr);
    dataset_exists=.FALSE.
    call h5lexists_f(file_id, location, dataset_exists, hdferr); call chkhdferr(hdferr);
    call H5FClose_f(file_id,hdferr); call chkhdferr(hdferr);
    call h5close_f(hdferr); call chkhdferr(hdferr);
  end subroutine datasetExists

  ! subroutine readHDF5int(data_out,location,filename)
  !   integer, intent(out) :: data_out
  !   integer, dimension(1) :: data
  !   integer(HSIZE_T), dimension(2) :: dims
  !   integer(HID_T) :: file_id
  !   integer(SIZE_T) :: i, j
  !   integer :: type_class
  !   integer(SIZE_T) :: type_size
  !   character(*), intent(in) :: location, filename
  !   integer :: hdferr
  !   call h5open_f(hdferr); call chkhdferr(hdferr);
  !   call H5FOpen_f(filename, H5F_ACC_RDONLY_F, file_id, hdferr); call chkhdferr(hdferr);
  !   call h5ltget_dataset_info_f(file_id, location, dims, type_class, type_size, hdferr);
  !   call chkhdferr(hdferr);

  !   call h5ltread_dataset_int_f(file_id, location, data, &
  !        dims, hdferr)
  !   data_out = data(1)
  !   call H5FClose_f(file_id,hdferr); call chkhdferr(hdferr);
  !   call h5close_f(hdferr); call chkhdferr(hdferr);

  ! end subroutine readHDF5int

  ! subroutine readHDF5dataset2D(data,size_in,location,filename)
  !   integer, intent(in), dimension(2) :: size_in
  !   real(kind=dp), dimension(size_in(1),size_in(2)), intent(out) :: data
  !   integer(HSIZE_T), dimension(2) :: dims
  !   integer(HID_T) :: file_id
  !   integer(SIZE_T) :: i, j
  !   integer :: type_class
  !   integer(SIZE_T) :: type_size
  !   character(*), intent(in) :: location, filename
  !   integer :: hdferr

  !   call h5open_f(hdferr); call chkhdferr(hdferr);
  !   call H5FOpen_f(filename, H5F_ACC_RDONLY_F, file_id, hdferr); call chkhdferr(hdferr);
  !   call H5LTget_dataset_info_f(file_id, location, dims, type_class, type_size, hdferr); call chkhdferr(hdferr);
  !   if ((dims(1).ne.size_in(1)).or.(dims(2).ne.size_in(2))) then
  !      write(*,*) "Error in readHDF5dataset: dimensions are wrong"
  !      write(*,*) "   dims = ", dims
  !      write(*,*) "   size = ", size_in
  !      stop
  !   end if
  !   data(:,:) = 0.0d00
  !   call H5LTread_dataset_double_f(file_id, location, data, dims, hdferr); call chkhdferr(hdferr);
  !   call H5FClose_f(file_id,hdferr); call chkhdferr(hdferr);
  !   call h5close_f(hdferr); call chkhdferr(hdferr);
  ! end subroutine readHDF5dataset2D

  ! subroutine readHDF5dataset2D_INT(data,size_in,location,filename)
  !   integer, intent(in), dimension(2) :: size_in
  !   integer, dimension(size_in(1),size_in(2)), intent(out) :: data
  !   integer(HSIZE_T), dimension(2) :: dims
  !   integer(HID_T) :: file_id
  !   integer(SIZE_T) :: i, j
  !   integer :: type_class
  !   integer(SIZE_T) :: type_size
  !   character(*), intent(in) :: location, filename
  !   integer :: hdferr

  !   call h5open_f(hdferr); call chkhdferr(hdferr);
  !   call H5FOpen_f(filename, H5F_ACC_RDONLY_F, file_id, hdferr); call chkhdferr(hdferr);
  !   call H5LTget_dataset_info_f(file_id, location, dims, type_class, type_size, hdferr); call chkhdferr(hdferr);
  !   if ((dims(1).ne.size_in(1)).or.(dims(2).ne.size_in(2))) then
  !      write(*,*) "Error in readHDF5dataset: dimensions are wrong"
  !      write(*,*) "   dims = ", dims
  !      write(*,*) "   size = ", size_in
  !      stop
  !   end if

  !   data(:,:) = 0
  !   call H5LTread_dataset_int_f(file_id, location, data, dims, hdferr); call chkhdferr(hdferr);

  !   call H5FClose_f(file_id,hdferr); call chkhdferr(hdferr);
  !   call h5close_f(hdferr); call chkhdferr(hdferr);
  ! end subroutine readHDF5dataset2D_INT



  ! subroutine writeHDF5dataset2D(data,size_in,location,filename)
  !   integer, intent(in), dimension(2) :: size_in
  !   real(kind=dp), dimension(size_in(1),size_in(2)), intent(in) :: data
  !   integer(HSIZE_T), dimension(2) :: dims
  !   integer(HID_T) :: file_id, dset_id, dspace_id
  !   integer(SIZE_T) :: i, j
  !   integer :: type_class
  !   integer(SIZE_T) :: type_size
  !   character(*), intent(in) :: location, filename
  !   integer :: hdferr

  !   dims = size_in
  !   call h5open_f(hdferr); call chkhdferr(hdferr);

  !   call H5Fopen_f(filename, H5F_ACC_RDWR_F, file_id, hdferr); call chkhdferr(hdferr);
  !   call H5Screate_simple_f(2, dims, dspace_id, hdferr); call chkhdferr(hdferr);
  !   call H5Dcreate_f(file_id, location, H5T_NATIVE_DOUBLE, dspace_id, dset_id, hdferr);
  !   CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, data, dims, hdferr); call chkhdferr(hdferr);
  !   call H5Dclose_f(dset_id, hdferr); call chkhdferr(hdferr);
  !   call H5Sclose_f(dspace_id, hdferr); call chkhdferr(hdferr);
  !   call H5Fclose_f(file_id, hdferr); call chkhdferr(hdferr);
  !   call H5close_f(hdferr);
  ! end subroutine writeHDF5dataset2D

  ! subroutine writeHDF5dataset1D(data,size_in,location,filename)
  !   integer, intent(in) :: size_in
  !   real(kind=dp), dimension(size_in), intent(in) :: data
  !   integer(HSIZE_T), dimension(1) :: dims
  !   integer(HID_T) :: file_id, dset_id, dspace_id
  !   integer(SIZE_T) :: i, j
  !   integer :: type_class
  !   integer(SIZE_T) :: type_size
  !   character(*), intent(in) :: location, filename
  !   integer :: hdferr

  !   dims(1) = size_in
  !   call h5open_f(hdferr); call chkhdferr(hdferr);

  !   call H5Fopen_f(filename, H5F_ACC_RDWR_F, file_id, hdferr); call chkhdferr(hdferr);
  !   call H5Screate_simple_f(1, dims, dspace_id, hdferr); call chkhdferr(hdferr);
  !   call H5Dcreate_f(file_id, location, H5T_NATIVE_DOUBLE, dspace_id, dset_id, hdferr);
  !   CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, data, dims, hdferr); call chkhdferr(hdferr);
  !   call H5Dclose_f(dset_id, hdferr); call chkhdferr(hdferr);
  !   call H5Sclose_f(dspace_id, hdferr); call chkhdferr(hdferr);
  !   call H5Fclose_f(file_id, hdferr); call chkhdferr(hdferr);
  !   call H5close_f(hdferr);
  ! end subroutine writeHDF5dataset1D


  ! subroutine writeHDF5dataset2D_new(data,size_in,location,filename)
  !   integer, intent(in), dimension(2) :: size_in
  !   real(kind=dp), dimension(size_in(1),size_in(2)), intent(in) :: data
  !   integer(HSIZE_T), dimension(2) :: dims
  !   integer(HID_T) :: file_id, dset_id, dspace_id
  !   integer(SIZE_T) :: i, j
  !   integer :: type_class
  !   integer(SIZE_T) :: type_size
  !   character(*), intent(in) :: location, filename
  !   integer :: hdferr

  !   dims = size_in
  !   ! call h5open_f(hdferr); call chkhdferr(hdferr);
  !   ! call H5FCreate_f(filename, H5F_ACC_RDWR_F, file_id, hdferr); call chkhdferr(hdferr)
  !   ! call H5FOpen_f(filename, H5F_ACC_RDWR_F, file_id, hdferr); 
  !   ! call H5LTmake_dataset_double_f(file_id, location, 2, dims, &
  !   !      data, hdferr); call chkhdferr(hdferr);
  !   ! call H5FClose_f(file_id,hdferr); call chkhdferr(hdferr);
  !   ! call h5close_f(hdferr); call chkhdferr(hdferr);
  !   call h5open_f(hdferr); call chkhdferr(hdferr);
  !   call H5Fcreate_f(filename, H5F_ACC_TRUNC_F, file_id, hdferr); call chkhdferr(hdferr);
  !   call H5Screate_simple_f(2, dims, dspace_id, hdferr); call chkhdferr(hdferr);
  !   call H5Dcreate_f(file_id, location, H5T_NATIVE_DOUBLE, dspace_id, dset_id, hdferr);
  !   CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, data, dims, hdferr); call chkhdferr(hdferr);
  !   call H5Dclose_f(dset_id, hdferr); call chkhdferr(hdferr);
  !   call H5Sclose_f(dspace_id, hdferr); call chkhdferr(hdferr);
  !   call H5Fclose_f(file_id, hdferr); call chkhdferr(hdferr);
  !   call H5close_f(hdferr);
  ! end subroutine writeHDF5dataset2D_new

  
  ! pure double precision function fcOverlapIntegral(vibGround,vibExcited,HR)
  !   implicit none
  !   integer, intent(in) :: vibExcited, vibGround
  !   double precision, intent(in) :: HR
  !   double precision :: HR_in
  !   integer :: M, N
  !   double precision :: dg, fctmp

  !   fcOverlapIntegral= 0.0d00
  !   ! if (vibExcited==0) then
  !   !    fcOverlapIntegral = sqrt(1.0/(1.0+HR))
  !   ! else if (vibExcited==1) then
  !   !    fcOverlapIntegral = -sqrt(HR/(1.0+HR))
  !   ! end if
    
  !   HR_in = HR/2.0d00
  !   !> Simplified formula for FC computation
  !   !! when vibGround == 0.
  !   !!

  !   !if (vibGround==0) then
  !   !    fcOverlapIntegral = (-1.0)**vibExcited * exp(-HR/2.0)*(HR**(vibExcited/2.0)/(fac(vibExcited)**0.5))
  !   !    return
  !   ! end if
 
  !   !> Complete formula for FC amplitude computation
  !   !! based on the ref. from above.

  !   do M=0, vibGround
  !      do N=0, vibExcited
  !         ! fctmp may as well be a NaN, so we first compute it, and then check it.
  !         if ((vibExcited-N)==(vibGround-M)) then
  !            fctmp = (((-1.0)**N * ((2.0*HR_in)**0.5)**(M+N))/(1.0d00*fac(M)*fac(N)))&
  !                 *((1.0d00*fac(vibExcited)*fac(vibGround))/(1.0d00*fac(vibExcited-N)*fac(vibGround-M)))**0.5
  !         else
  !            fctmp = 0.0d00
  !            continue
  !         end if
  !         if (.not.isnan(fctmp)) then
  !            fcOverlapIntegral = fcOverlapIntegral + fctmp
  !         else ! If we're getting a NaN, we're not adding it to the sum, just assume that it's too small to be added.
  !            continue
  !         end if
  !      end do
  !   end do
  !   fcOverlapIntegral = fcOverlapIntegral * exp(-1.0d00*HR_in)
  ! end function fcOverlapIntegral

  ! pure integer function fac(m) !
  !   integer, intent(in) :: m
  !   integer i

  !   if (m==0) then
  !      fac = 1
  !   else if (m>0) then
  !      fac = 1
  !      do i=1,m
  !         fac = fac*i
  !      end do
  !   end if
  !   return
  ! end function fac

end module dynamics_rtlib
