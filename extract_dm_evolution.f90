program extract_dm_evolution
  use dynamics_rtlib
  use dynamics_datatypes, only: dp
  use dynamics_errchk
  use extract_dm_io
  use extract_dm_rt
  use io
  implicit none
  real(kind=dp), allocatable, dimension(:,:) :: rho_ij_re
  real(kind=dp), allocatable, dimension(:,:) :: population_monomers
  real(kind=dp), allocatable, dimension(:,:) :: population_current
  real(kind=dp), allocatable, dimension(:,:) :: rho_ij_im
  real(kind=dp), allocatable, dimension(:,:) :: rho_current
  real(kind=dp), allocatable, dimension(:) :: timeline
  integer :: i, j, k, stepnumber, total_steps, ierr, Nmers
  integer, dimension(2) :: dims_rho, dims_t, dims_nmers
  character(17) :: rho_name
  character(20) :: monomers_name
  character(15) :: population_name
  character(15) :: rho_ij_name
  logical :: problem, dset_exists

  write(*,"(A)") "  * Extracting density matrix evolution * "

  ! ### First, read how many density matricies are there ###
  stepnumber=1
  total_steps=0
  do
     write(rho_name,"(A7,I10.10)") "/rho_re",stepnumber
     call datasetExists(dataset_exists=dset_exists, location=rho_name, filename="DMpropagation.h5")
     if (dset_exists) then
        stepnumber = stepnumber + 1
        total_steps=total_steps+1
        continue
     else
        exit
     end if
  end do

  write(*,*) " Total number of DMs: ", total_steps

  ! ### Second, get an idea of what to read ###
  call parseInputFile()

  ! ### Set up internal data in the modules
  
  ! ### Third, loop over the DMs and read what you need ###
  write(*,*) " * Getting ready to read the data * "

  ! # Read the timeline
  call readHDF5dataset_size(dims_t, location="/t", filename="DMpropagation.h5")
  allocate(timeline(dims_t(1)),stat=ierr); call chkmerr(ierr)
  
  call readHDF5dataset2D(timeline,dims_t,location="/t",filename="DMpropagation.h5")
  
  ! # Create new HDF5 file and store timeline there
  call writeHDF5dataset2D_new(timeline,dims_t,location="/t",filename="rho_components.h5")

  ! # Read the size of DM
  stepnumber=0
  write(rho_name,"(A7,I10.10)") "/rho_re",stepnumber
  call readHDF5dataset_size(dims_rho, location=rho_name, filename="DMpropagation.h5")

  allocate(rho_current(dims_rho(1),dims_rho(2)),stat=ierr); call chkmerr(ierr)
  allocate(rho_ij_re(total_steps+1,exrt%max_rho_idx),stat=ierr); call chkmerr(ierr)
  allocate(rho_ij_im(total_steps+1,exrt%max_rho_idx),stat=ierr); call chkmerr(ierr)

  dims_nmers(:) = 0
  call readHDF5dataset_size(dims_nmers, location="/monomers_0000000000", filename="DMpropagation.h5")
  Nmers = dims_nmers(1)
  write(*,*) "Nmers = ", Nmers, "dims = ", dims_nmers
  allocate(population_current(dims_nmers(1),dims_nmers(2)),stat=ierr); call chkmerr(ierr)
  allocate(population_monomers(total_steps+1,Nmers),stat=ierr); call chkmerr(ierr)
  
  ! # Loop over all components, and save them
  do stepnumber=0,total_steps
     ! # Read the density matrix: real part
     write(rho_name,"(A7,I10.10)") "/rho_re",stepnumber
     call readHDF5dataset2D(rho_current,dims_rho,location=rho_name,filename="DMpropagation.h5")
     do i=1,exrt%max_rho_idx
        rho_ij_re(stepnumber+1,i) = rho_current(exrt%rho_idx(i,1),exrt%rho_idx(i,2))
     end do
     ! # Read the density matrix: imaginary part
     write(rho_name,"(A7,I10.10)") "/rho_im",stepnumber
     call readHDF5dataset2D(rho_current,dims_rho,location=rho_name,filename="DMpropagation.h5")
     do i=1,exrt%max_rho_idx
        rho_ij_im(stepnumber+1,i) = rho_current(exrt%rho_idx(i,1),exrt%rho_idx(i,2))
     end do

     ! # Read the monomer's population
     write(monomers_name,"(A10,I10.10)") "/monomers_",stepnumber
     call readHDF5dataset2D(population_current,dims_nmers,location=monomers_name,filename="DMpropagation.h5")
     do i=1,Nmers
        population_monomers(stepnumber+1,i) = population_current(i,1)
     end do

  end do
  do i=1,exrt%max_rho_idx
     ! # Write the i-th component of the DM
     write(rho_ij_name,"(A8,I3.3,A1,I3.3)") "/rho_re_",exrt%rho_idx(i,1),"_",exrt%rho_idx(i,2)
     call writeHDF5dataset1D(rho_ij_re(:,i),total_steps+1,location=rho_ij_name,filename="rho_components.h5")
     write(rho_ij_name,"(A8,I3.3,A1,I3.3)") "/rho_im_",exrt%rho_idx(i,1),"_",exrt%rho_idx(i,2)
     call writeHDF5dataset1D(rho_ij_im(:,i),total_steps+1,location=rho_ij_name,filename="rho_components.h5")
  end do
  do i=1,Nmers
     write(population_name,"(A12,I3.3)") "/population_",i
     call writeHDF5dataset1D(population_monomers(:,i),total_steps+1,location=population_name,filename="rho_components.h5")
  end do
    
end program extract_dm_evolution

