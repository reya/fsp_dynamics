module extract_dm_io
  use extract_dm_rt
  implicit none

  public :: parseInputFile
  private 
contains
  
subroutine parseInputFile()
  ! We read input lines in buffer and label vars.
  integer :: ios = 0, line = 0, funit=10, pos ! Note that funit=10 is arbitrary. It could be any unit you want.
  integer :: ierr
  integer :: i, j, k
  integer, dimension(1000,2) :: rho_idx_initial
  integer :: max_rho_idx, idx, jdx
  character(len=128) :: buffer, label    
  character(len=32)  :: filename

  ! ### Now we're going to re-use input parser we already have
  print *, " * Reading the input * "
  !
  ! ##########################
  ! ### INPUT PARSER BLOCK ###
  ! ##########################
  ! Get the input file name as a first command-line argument.
  max_rho_idx = 0
  rho_idx_initial(:,:) = 0
  call get_command_argument(1, filename)
  ! Open it
  open(funit, file=filename)
  ! Iterate over the keywords in the filename for the first time and get non-array parameters
  print *, " * Routine will extract DM evolution for the following indicies: "
  do while (ios == 0) ! while file still has lines to read
     read(funit, '(A)', iostat=ios) buffer ! we read the line and put it in the buffer
     if (ios == 0) then  ! And if ios == 0, there is a line. If it's not, file has ended.
        line = line + 1   
        pos = scan(buffer, ' ') ! find where a space first occurs
        label = buffer(1:pos)   ! split the buffer into the label, which is before a space
        buffer = buffer(pos+1:) ! and into the value, which is after the space. 
        select case (label)     ! Now let's iterate over all possible labels
        case ('rho_idx')
           max_rho_idx = max_rho_idx + 1
           read(buffer, *, iostat=ios) idx, jdx
           print "(A,I4,I4,2F12.6)", '  I, J = ',  idx, jdx
           rho_idx_initial(max_rho_idx,1) = idx
           rho_idx_initial(max_rho_idx,2) = jdx
        case default
           print *, "  * * * Skipping line in the input: " , line
        end select
     end if
  end do
  print *, "   End of input     * "
  
  ! # After reading the initial DM, we set up the initial conditions
  allocate(exrt%rho_idx(max_rho_idx,2))
  exrt%rho_idx(:,1) = rho_idx_initial(1:max_rho_idx,1)
  exrt%rho_idx(:,2) = rho_idx_initial(1:max_rho_idx,2)
  exrt%max_rho_idx = max_rho_idx
end subroutine parseInputFile

end module extract_dm_io
