module extract_dm_rt

  implicit none

  type node_int_2D
     type(node_int_2D), pointer :: next, prev
     integer, dimension(2) :: data
  end type node_int_2D

  type exrt_type
     integer, allocatable :: rho_idx(:,:)
     integer :: max_rho_idx
  end type exrt_type
  
  
  type(exrt_type) :: exrt
  
  public :: node_int_2D, exrt
  private
  
contains

end module extract_dm_rt
