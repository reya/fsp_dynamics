#coding: utf8
import sys
import matplotlib.pyplot as plt
import matplotlib
import h5py
import numpy as np
import seaborn as sns
import argparse
import os
import xarray as xr

def rho_label(rho_key):
    for i in xrange(1,100):
        if str(i) in rho_key:
            return r"$\rho_{"+str(i)+str(i)+r"}$"

def cm_to_in(x):
    return x*0.393701

def plot_stuff():
    RHO_DIR = os.getcwd()
    rho_filename="rho_components.h5"
    dm_prop="DMpropagation.h5"
#    if RHO_DIR==" ":
#        rho_filename="./rho_components.h5"
#        dm_prop = './DMpropagation.h5'
    # else:
    #     rho_path="./"+RHO_DIR
    #     rho_filename = rho_path+'/rho_components.h5'
    #     dm_prop = rho_path+'/DMpropagation.h5'
    #     dm_prop_file = h5py.File(dm_prop,'r')
    sns.set_style("white")

    matplotlib.rcParams.update({"font.size":10})
    fig = plt.figure(figsize=[cm_to_in(15),cm_to_in(8)])
    #fig = plt.figure(figsize=[cm_to_in(8),cm_to_in(8)])
    #sns.set(rc={"figure.figsize":(12,6)})
    print " I'm in: ", RHO_DIR
    # Iterate over keys to find out how much rho's are there
    rho_number = 0
        
    f = h5py.File(dm_prop,'r')
    rho = f.get("rho_re0000000000");
    t   = f.get("t");
    t   = np.array(t);    t = t[0];
    t = t / 41.341 # convert to fs
    rho_size = np.size(rho,0)
    t_size = np.size(t)
    t_range = t
    i_range = np.array(range(1,rho_size+1))
    j_range = np.array(range(1,rho_size+1))
    print "rho_size =", rho_size, "t_size = ", t_size
    print "i_range", i_range
    data_rho_re  = xr.DataArray(np.zeros([rho_size,rho_size,t_size]),dims=('i','j','t'),coords={'i':np.array(range(1,rho_size+1)),'j':np.array(range(1,rho_size+1)),'t':t})
    data_rho_im  = xr.DataArray(np.zeros([rho_size,rho_size,t_size]),dims=('i','j','t'),coords={'i':np.array(range(1,rho_size+1)),'j':np.array(range(1,rho_size+1)),'t':t})
    f = h5py.File(dm_prop,'r')
    monomers = f.get("monomers_0000000000")
    print monomers
    number_of_monomers = np.size(np.array(monomers))
    data_monomers  = xr.DataArray(np.zeros([number_of_monomers,t_size]),dims=('N','t'),coords={'N':np.array(range(1,number_of_monomers+1)),'t':t})

    for monomer_key in f.keys():
        if "monomers" in monomer_key:
            tmp = monomer_key.split("_")
            rho = f.get(monomer_key)
            rho = np.array(rho)
            data_monomers.loc[{'t':t[int(tmp[1])]}] = rho

    print data_monomers
    print "rho size = ", rho_size

    for rho_key in f.keys():
        print rho_key
        if "rho_re" in rho_key:
            rho = f.get(rho_key)
            rho = np.array(rho)
            tmp = rho_key.split("_re")
            data_rho_re.loc[{'i':i_range,'j':j_range,'t':t_range[int(tmp[1])]}]=rho
        if "rho_im" in rho_key:
            rho = f.get(rho_key)
            rho = np.array(rho)
            tmp = rho_key.split("_im")
            data_rho_im.loc[{'i':i_range,'j':j_range,'t':t_range[int(tmp[1])]}]=rho
    
    
    print "RHO_NUMBER = ", rho_size
    rho_number = rho_size
    if (rho_number>6):
        sns.set_palette(sns.husl_palette(rho_number,l=.55))
    elif (rho_number>0):
        sns.set_palette(sns.color_palette('deep',rho_number))
    else:
        sns.set_palette(sns.color_palette('deep'))

    if RUNTYPE=="population":
        for i in i_range:
            plt.plot(t,data_rho_re.loc[{'i':i,'j':i}],linewidth=1.5,label=r"$\rho_{"+str(i)+"\,"+str(i)+r"}$")
        plt.ylim([0,data_rho_re.max()])

    if RUNTYPE=="monomers":
        for i in range(1,number_of_monomers+1):
            plt.plot(t,data_monomers.loc[{'N':i}],linewidth=1.5,label=r"$p_{"+str(i)+r"}$")
        plt.ylim([0,1])
        
    plt.xlim([0,np.max(t)+10])

        


RUNTYPE=" "
if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--runtype',choices=["monomers","population"], help="Either plot monomer populations, or eigenstate populations")

    args = parser.parse_args()
    RUNTYPE = args.runtype

    plot_stuff()
    plt.grid()

    if RUNTYPE=="population":
        plt.xlabel(u"Время, фс ")
        plt.ylabel(u"Населённость, "+r"$\rho_{ii}$")
    if RUNTYPE=="monomers":
        plt.xlabel(u"Время, фс ")
        plt.ylabel(u"Населенность на мономерах, "+r"$p_{i}$")
        
    plt.tight_layout()
    plt.legend(loc='best',ncol=4)
    NAME = args.runtype
    plt.savefig(NAME+".png",format="png",dpi=600)
    plt.savefig(NAME+".eps",format="eps")
    plt.savefig(NAME+".svg",format="svg")
    plt.savefig("output.png",format="png",dpi=300)
    
        
    
    
